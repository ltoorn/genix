/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

import ltoorn.genix.context.Context;
import ltoorn.genix.context.configuration.ConfigOption;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Contracts;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Generating the index involves the following steps:
 * <ul>
 *     <li>The {@link IndexElement}s must be sorted alphabetically.</li>
 *     <li>Each element must be pretty printed, see {@link PrettyPrinter}.</li>
 *     <li>If so specified in the user configuration, (sub) header and/or extra spacing must be added,
 *     delimiting different (alphabetical) sections.</li>
 * </ul>
 */
public class IndexGenerator {

    private IndexGenerator() {}

    /**
     * Generates the index. It partitions the index to enable paragraph spacing between different alphabetical sections
     * in the index, and calls the pretty printer on all partitions to produce the actual output. The list of index
     * elements is also sanitized to make sure no elements with empty page number lists, or elements with reference
     * to elements with empty page number lists, and so on, will be sent to the final output.
     * @param indexElements The list of index elements we need to send to the output.
     * @return The pretty printed output.
     */
    public static String generateIndex(List<IndexElement> indexElements) {
        StringBuilder builder = new StringBuilder();
        UserConfig config = Context.get().config;
        List<List<IndexElement>> partitions = partition(IndexElementsPreProcessor.finalizeIndexElements(indexElements));
        for (List<IndexElement> partition : partitions) {
            builder.append(config.getIndexHeaderSpacing());
            for (IndexElement element : partition) {
                String output = PrettyPrinter.prettyPrint(element, config);
                builder.append(output);
            }
        }
        return builder.toString();
    }

    /**
     * Partitions the list of index elements according to alphabetical grouping.
     * @param indexElements The list of index elements based on which we're generating the output.
     * @return The partitioned list of index elements.
     */
    private static List<List<IndexElement>> partition(List<IndexElement> indexElements) {
        final List<List<IndexElement>> partitions = new ArrayList<>();
        List<IndexElement> partition = new ArrayList<>();

        int current = 0;
        for (IndexElement element : indexElements) {
            int next = Character.toLowerCase(element.contentForSearch.codePointAt(0));
            if (Character.isAlphabetic(next) && next != current) {
                if (!partition.isEmpty()) {
                    partitions.add(partition);
                    partition = new ArrayList<>();
                }
                current = next;
            }
            partition.add(element);
        }
        // Add the final partition, if it's not empty.
        if (!partition.isEmpty()) {
            partitions.add(partition);
        }
        return partitions;
    }

    /**
     * The Pretty Printer must take care of the following tasks:
     * <ul>
     *     <li>The page numbers must be formatted according to the user configuration.</li>
     *     <li>If so specified in the user configuration, a certain range of the page numbers must be translated to roman numerals.</li>
     *     <li>For each element type the pretty printer defines and implements specific printing rules.</li>
     *     <li>Each {@link IndexElement} with at least one occurrence in the text (i.e. the size of
     *     {@link IndexElement#getPages()} > 0), must be pretty printed in order according to the pretty printing rules of
     *     its element type, building up and returning the entire index.</li>
     * </ul>
     */
    private static class PrettyPrinter {

        private static final Function<String, String> appendNewLine = s -> s + "\n";
        private static final Function<String, String> appendSpace = s -> s + " ";
        private static final Function<String, String> wrapSpace = s -> " " + s + " ";

        private PrettyPrinter() {}

        /**
         * Pretty prints the {@code element} according to the pretty printing rules defined for
         * its attributes, and the user defined options in {@link UserConfig}.
         * <ul>
         *     <li>Simple elements are printed as the element name followed by the list of page numbers
         *     on which the element occurs.</li>
         *     <li>Elements with reference are printed as the element name followed by the name
         *     of the element it references.</li>
         *     <li>Elements with members are printed as the parent element name followed by an
         *     indented section of member elements (per line), each printed according to their type.</li>
         *     <li>Elements with synonyms are printed as the parent element name followed by the combined
         *     list of page numbers on which it and its synonyms occur (without duplicates).</li>
         * </ul>
         *
         * @param element The {@link IndexElement} to be printed.
         * @param config  The {@link UserConfig} which defines, among other things, what delimiter symbols are
         *                used, and whether any sublist of the page numbers needs to be represented as Roman numerals.
         *                For o general overview of the relevant user configuration options see README.adoc, for
         *                a more detailed (informal) specification see spec.adoc (in the root directory), and for
         *                implementation details see {@link ConfigOption} and {@link UserConfig}.
         * @return The pretty printed {@code element}.
         */
        private static String prettyPrint(IndexElement element, UserConfig config) {
            Contracts.assumeAll(Checks.isNotNull(element), Checks.isNotNull(config));
            return prettyPrint(element, config, 0);
        }

        private static String prettyPrint(IndexElement element, UserConfig config, int currentIndentationLevel) {
            if (element.hasMembers()) return printElementWithMembers(element, config, currentIndentationLevel);
            else if (element.hasHardReference()) return printElementWithHardReference(element, config);
            else return printSimpleElement(element, config);
        }

        private static String printSimpleElement(IndexElement element, UserConfig config) {
            Contracts.assume(Checks.isNotNullOrEmptyCollection(element.getPages()));
            return element.contentForOutput
                    + appendSpace.apply(config.getPageNumbersStartDelimiter())
                    + printPageNumbersWithNewLine(element, config);
        }

        private static String printElementWithHardReference(IndexElement element, UserConfig config) {
            Contracts.assumeAll(Checks.isNotNull(element.getHardReference()),
                    Checks.isNotNullOrBlankString(element.getHardReference().contentForOutput));

            StringBuilder builder = new StringBuilder();
            builder.append(element.contentForOutput)
                    .append(config.getPageNumbersStartDelimiter())
                    .append(wrapSpace.apply(config.getHardReferenceDelimiter()))
                    .append(element.getHardReference().contentForOutput);

            if (element.hasSoftReference()) {
                appendSoftReferences(builder, element, config);
            }

            return appendNewLine.apply(builder.toString());
        }

        private static String printElementWithMembers(IndexElement element, UserConfig config, int currentIndentationLevel) {
            Contracts.assumeAll(Checks.isNotNullOrBlankString(element.contentForOutput),
                    Checks.isNotNullOrEmptyCollection(element.getMembers()));

            int nextIndentationLevel = currentIndentationLevel + 1;
            String parent = appendNewLine.apply(printParentElement(element, config));
            StringBuilder builder = new StringBuilder(parent);
            for (IndexElement member : element.getMembers()) {
                String printedMember = printMember(member, config, nextIndentationLevel);
                builder.append(printedMember);
            }
            return builder.toString();
        }

        private static String printParentElement(IndexElement element, UserConfig config) {
            StringBuilder builder = new StringBuilder(element.contentForOutput);

            if (element.isIndexable() && Checks.isNotNullOrEmptyCollection(element.getPages())) {
                builder.append(appendSpace.apply(config.getPageNumbersStartDelimiter()))
                        .append(printPageNumbers(element, config));
            }

            if (element.hasSoftReference()) {
                builder.append(config.getPageNumberSeparator());
                appendSoftReferences(builder, element, config);
            }

            return builder.toString();
        }

        private static String printMember(IndexElement member, UserConfig config, int indentationLevel) {
            StringBuilder indentation = new StringBuilder();
            for (int i = 0; i < indentationLevel; ++i) {
                indentation.append("\t");
            }
            String lineContent = prettyPrint(member, config, indentationLevel);
            indentation.append(lineContent);
            return indentation.toString();
        }

        private static String printPageNumbersWithNewLine(IndexElement element, UserConfig config) {
            return appendNewLine.apply(printPageNumbers(element, config));
        }

        private static String printPageNumbers(IndexElement element, UserConfig config) {
            Contracts.assume(Checks.isNotNullOrEmptyCollection(element.getPages()));

            StringBuilder builder = new StringBuilder();
            List<PageNumberSeq> pageNumberSeqs = Conversion.toPageNumberSeqs(element.getPages());

            boolean first = true;
            for (PageNumberSeq seq : pageNumberSeqs) {
                if (first) {
                    builder.append(seq.print());
                    first = false;
                    continue;
                }
                builder.append(appendSpace.apply(config.getPageNumberSeparator()));
                builder.append(seq.print());
            }

            if (element.hasSoftReference()) {
                builder.append(config.getPageNumberSeparator());
                appendSoftReferences(builder, element, config);
            }

            return builder.toString();
        }

        private static void appendSoftReferences(StringBuilder builder, IndexElement element, UserConfig config) {
            builder.append(wrapSpace.apply(config.getSoftReferenceDelimiter()));
            List<IndexElement> softReferences = element.getSoftReferences();
            for (int i = 0; i < softReferences.size(); i++) {
                builder.append(softReferences.get(i).contentForOutput);
                if (i < softReferences.size() - 1) {
                    builder.append(appendSpace.apply(config.getPageNumberSeparator()));
                }
            }
        }
    }
}
