/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

import ltoorn.genix.context.Context;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Contracts;
import ltoorn.genix.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Provides utility methods for converting between different data types and representations
 * used in the generation of an index.
 */
public final class Conversion {

    private Conversion() {}

    /**
     * Converts the given {@code number} to its roman numeral representation. For
     * numbers >= 4000 no special construction is used, i.e. large numbers will
     * result in a proportionally long prefix of m's. The resulting representation
     * is always in lowercase; it's up to the consumer to translate it to uppercase
     * if necessary.
     * @param number The number to be converted; The {@code number} is assumed to be > 0
     * @return The Roman numeral representation of the {@code number}
     */
    public static String toRomanNumeral(int number) {
        Contracts.assume(number > 0);
        final String romanNumeral = toRomanNumeralHelper(number, new StringBuilder());
        return Contracts.require(romanNumeral, Checks::isNotNullOrBlankString);
    }

    /**
     * Converts the given list of {@code pageNumbers} to a list of {@link PageNumberSeq}.
     * @param pageNumbers The list of page numbers to be converted; the list is assumed to be non-empty
     * @return The converted list of {@link PageNumberSeq}
     */
    public static List<PageNumberSeq> toPageNumberSeqs(List<Integer> pageNumbers) {
        Contracts.assume(Checks.isNotNullOrEmptyCollection(pageNumbers));
        final List<Integer> corrected = pageNumbers.stream().map(Conversion::computeCorrectedPageNumber).collect(Collectors.toList());

        if (Checks.isNotNull(Context.get().config.getRomanNumeralEnd())) {
            Pair<List<Integer>, List<Integer>> partitions = partitionForRomanNumeralConversion(corrected);
            List<PageNumberSeq> left = toPageNumberSeqsHelper(partitions.getLeft(), true);
            List<PageNumberSeq> right = toPageNumberSeqsHelper(partitions.getRight(), false);
            left.addAll(right);
            return Contracts.require(left, Checks::isNotNullOrEmptyCollection);
        }

        return Contracts.require(toPageNumberSeqsHelper(corrected, false), Checks::isNotNullOrEmptyCollection);
    }

    private static List<PageNumberSeq> toPageNumberSeqsHelper(List<Integer> pageNumbers, boolean isRoman) {
        List<Integer> buffer = new ArrayList<>();
        List<List<Integer>> groupings = new ArrayList<>();
        for (Integer pn : pageNumbers) {
            // If the last element in the buffer does not equal the page number `pn - 1` of the
            // current iteration, then the sequence is broken. We add the sequence we've collected
            // so far to the groupings list, and initiate a new buffer to collect the next
            // sequence, starting with the `pn` of the current iteration.
            if (!buffer.isEmpty() && last(buffer) != (pn-1)) {
                groupings.add(buffer);
                buffer = new ArrayList<>();
            }
            buffer.add(pn);
        }
        // Upon exiting the loop, there may be a final sequence left over. If that's the case
        // we add it to the list.
        if (!buffer.isEmpty()) {
            groupings.add(buffer);
        }

        ArrayList<PageNumberSeq> seqs = new ArrayList<>();
        for (List<Integer> grouping : groupings) {
            final RegularSeq seq;
            if (grouping.size() == 1) {
                seq = RegularSeq.singleton(first(grouping));
            } else {
                seq = RegularSeq.of(first(grouping), last(grouping));
            }
            seqs.add(isRoman ? seq.toRoman() : seq);
        }
        return seqs;
    }

    private static Pair<List<Integer>, List<Integer>> partitionForRomanNumeralConversion(List<Integer> pageNumbers) {
        final Integer romanNumeralEnd = Context.get().config.getRomanNumeralEnd();
        final List<Integer> romanNumerals = new ArrayList<>();
        final List<Integer> remainingNumerals = new ArrayList<>();

        for (Integer i : pageNumbers) {
            if (i > romanNumeralEnd) {
                // After the roman numerals, the normal page numbers start at 1 again, so we need
                // to correct for the difference.
                int correctedPageNumber = i - romanNumeralEnd;
                remainingNumerals.add(correctedPageNumber);
            } else {
                romanNumerals.add(i);
            }
        }

        return Pair.of(romanNumerals, remainingNumerals);
    }

    /**
     * Computes the corrected page number by adding 1 to the {@code absolutePageNumber}. We need to do
     * this to correct for the zero based indices we use to keep track of the pages internally.
     * @param absolutePageNumber The absolute page number (i.e. the index in the list of pages extracted from the PDF).
     * @return The corrected page number.
     */
    private static Integer computeCorrectedPageNumber(Integer absolutePageNumber) {
        return absolutePageNumber + 1;
    }

    private static String toRomanNumeralHelper(int number, StringBuilder prefix) {
        final char i_1 = 'i';
        final char v_5 = 'v';
        final char x_10 = 'x';
        final char l_50 = 'l';
        final char c_100 = 'c';
        final char d_500 = 'd';
        final char m_1000 = 'm';

        if (number >= 1000) return toRomanNumeralHelper(number-1000, prefix.append(m_1000));
        else if (number >= 900) return toRomanNumeralHelper(number-900, prefix.append(c_100).append(m_1000));
        else if (number >= 500) return toRomanNumeralHelper(number-100, prefix.append(d_500));
        else if (number >= 400) return toRomanNumeralHelper(number-400, prefix.append(c_100).append(d_500));
        else if (number >= 100) return toRomanNumeralHelper(number-100, prefix.append(c_100));
        else if (number >= 90) return toRomanNumeralHelper(number-90, prefix.append(x_10).append(c_100));
        else if (number >= 50) return toRomanNumeralHelper(number-50, prefix.append(l_50));
        else if (number >= 40) return toRomanNumeralHelper(number-40, prefix.append(x_10).append(l_50));
        else if (number >= 10) return toRomanNumeralHelper(number-10, prefix.append(x_10));
        else if (number == 9) return toRomanNumeralHelper(0, prefix.append(i_1).append(x_10));
        else if (number >= 5) return toRomanNumeralHelper(number-5, prefix.append(v_5));
        else if (number == 4) return toRomanNumeralHelper(0, prefix.append(i_1).append(v_5));
        else if (number >= 1) return toRomanNumeralHelper(number-1, prefix.append(i_1));
        else return prefix.toString();
    }

    private static <T> T first(List<T> list) {
        Contracts.assume(Checks.isNotNullOrEmptyCollection(list), "Can't get first element of empty list");
        return list.get(0);
    }

    private static <T> T last(List<T> list) {
        Contracts.assume(Checks.isNotNullOrEmptyCollection(list), "Can't get last element of empty list");
        return list.get(list.size()-1);
    }
}
