/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

import ltoorn.genix.context.Context;

/**
 * @inheritDoc
 *
 * This implementation of the {@link PageNumberSeq} interface handles the
 * encoding of the page number sequences in Roman numerals.
 */
public class RomanSeq implements PageNumberSeq {

    private final int start;
    private final int end;
    private final boolean isSingleton;

    private RomanSeq(int start, int end, boolean isSingleton) {
        this.start = start;
        this.end = end;
        this.isSingleton = isSingleton;
    }

    /**
     * Constructs a {@code RomanSeq} of the provided {@code start} and {@code end} values.
     * @param start The first value of the sequence.
     * @param end The last value of the sequence.
     * @return The new {@code RomanSeq}.
     */
    public static RomanSeq of(int start, int end) {
        return new RomanSeq(start, end, false);
    }

    /**
     * Constructs a singleton {@code RomanSeq} of the provided {@code val}.
     * @param val The value of the singleton sequence.
     * @return The new singleton {@code RomanSeq}.
     */
    public static RomanSeq singleton(int val) {
        return new RomanSeq(val, val, true);
    }

    /**
     * @inheritDoc
     *
     * Converts the start of the sequence to Roman numerals before returning it. If so
     * specified in the {@link ltoorn.genix.context.configuration.UserConfig} the Roman
     * numeral is also converted to uppercase.
     * @return The Roman numeral representation of the start of the sequence.
     */
    @Override
    public String printStart() {
        String romanStart = Conversion.toRomanNumeral(start);
        return Context.get().config.getRomanNumeralUpperCase() ? romanStart.toUpperCase() : romanStart;
    }

    /**
     * @inheritDoc
     *
     * Converts the end of the sequence to Roman numerals before returning it. If so
     * specified in the {@link ltoorn.genix.context.configuration.UserConfig} the Roman
     * numeral is also converted to uppercase.
     * @return The Roman numeral representation of the end of the sequence.
     */
    @Override
    public String printEnd() {
        String romanEnd = Conversion.toRomanNumeral(end);
        return Context.get().config.getRomanNumeralUpperCase() ? romanEnd.toUpperCase() : romanEnd;
    }

    @Override
    public boolean isSingleton() {
        return isSingleton;
    }
}
