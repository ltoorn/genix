/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

import ltoorn.genix.context.Context;
import ltoorn.genix.dsl.specification.ir.IndexElement;

/**
 * A {@code PageNumberSeq} encodes a sequence of subsequent page numbers (of occurrences)
 * for a given {@link IndexElement}.
 */
public interface PageNumberSeq {

    /**
     * Prints the first number (inclusive) of the sequence.
     * @return The starting number as a String.
     */
    String printStart();

    /**
     * Prints the last number (inclusive) of the sequence.
     * @return The last number as a String.
     */
    String printEnd();

    /**
     * Denotes whether the {@code PageNumberSeq} is a singleton. A singleton is a sequence of
     * one element. For any singleton sequence, {@link #printStart()} == {@link #printEnd()}.
     * @return {@code true} if the {@code PageNumberSeq} is a singleton, {@code false} otherwise.
     */
    boolean isSingleton();

    /**
     * Prints the {@code PageNumberSeq}. Singleton sequences are printed as single numbers
     * by calling {@link #printStart()}, non-singleton sequences are printed by concatenating
     * {@link #printStart()} with the user defined sequence connector and {@link #printEnd()}.
     * @return The String representation of the entire sequence.
     */
    default String print() {
        return isSingleton()
               ? printStart()
               : printStart() + Context.get().config.getSequenceConnector() + printEnd();
    }
}
