/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

import ltoorn.genix.dsl.specification.ir.AttributeKey;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.io.Output;
import ltoorn.genix.util.Checks;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IndexElementsPreProcessor {

    private static final Comparator<String> comparator = Comparator.comparing(String::toLowerCase);

    private IndexElementsPreProcessor() {}

    public static List<IndexElement> finalizeIndexElements(List<IndexElement> elements) {
        ArrayList<IndexElement> processedElements = new ArrayList<>();
        elements.forEach(IndexElementsPreProcessor::finalizeSynonyms);
        for (IndexElement element : elements) {
            if (hasOccurrence(element)) {
                IndexElement prunedElement = pruneSoftReference(pruneElementWithMembers(element));
                processedElements.add(prunedElement);
            } else {
                Output.printMessage(
                        Output.MessageType.WARNING
                        , "[line: "
                                + element.lineNumber
                                + "] "
                                + "The following element is part of the specification, but no occurrences for it were found in the text: '"
                                + element.contentForOutput
                                + "'."
                );
            }
        }
        processedElements.sort((l, r) -> comparator.compare(l.contentForSearch, r.contentForSearch));
        return processedElements;
    }

    private static void finalizeSynonyms(IndexElement element) {
        if (element.hasSynonyms() && element.isIndexable()) {
            mergeSynonyms(element);
        }
        if (element.hasMembers()) {
            element.getMembers().forEach(IndexElementsPreProcessor::finalizeSynonyms);
        }
    }

    private static boolean hasOccurrence(IndexElement element) {
        if (element.isIndexable() && Checks.isNotNullOrEmptyCollection(element.getPages())) {
            return true;
        } else if (element.hasMembers()) {
            // If the element has any non-empty children we consider the element non-empty
            return element.getMembers().stream().anyMatch(IndexElementsPreProcessor::hasOccurrence);
        } else if (element.hasHardReference()) {
            return hasOccurrence(element.getHardReference());
        } else {
            // Because we treat soft references as secondary, whether an element has any occurrences can never depend
            // on the occurrences of its soft reference. So if an element doesn't have any occurrences of its own, but
            // its soft reference does, hasOccurrence will still evaluate to false.
            return false;
        }
    }

    /**
     * Prunes the elements with members. Any member that doesn't have any occurrences in the text
     * will be removed from the member list. Since member lists can be arbitrarily nested with
     * other elements with members, this method is recursively called on all members before adding
     * them to the pruned list.
     * <p>This method must also sort the pruned member list, since the sort of the top level elements
     * doesn't reach into the member lists.</p>
     * <p>Elements that don't have members will be returned unchanged.</p>
     * @param element The element we will prune.
     * @return The pruned element.
     */
    private static IndexElement pruneElementWithMembers(IndexElement element) {
        if (element.hasMembers()) {
            final List<IndexElement> prunedMembers = new ArrayList<>();
            for (IndexElement member : element.getMembers()) {
                if (hasOccurrence(member)) {
                    prunedMembers.add(pruneElementWithMembers(member));
                } else {
                    Output.printMessage(
                            Output.MessageType.WARNING
                            , "[line: "
                                    + member.lineNumber
                                    + "] "
                                    + "The following member element is part of the specification, but no occurrences for it were found in the text: '"
                                    + member.contentForOutput
                                    + "', of parent: '"
                                    + element.contentForOutput
                                    + "'."
                    );
                }
            }
            element.getMembers().clear();
            prunedMembers.sort((l, r) -> comparator.compare(l.contentForSearch, r.contentForSearch));
            element.getMembers().addAll(prunedMembers);
        }
        return element;
    }

    private static IndexElement pruneSoftReference(IndexElement element) {
        if (element.hasSoftReference()) {
            List<IndexElement> softReferences = element.getSoftReferences();
            element.dissoc(AttributeKey.SOFT_REFERENCE);
            for (IndexElement softReference : softReferences) {
                if (hasOccurrence(softReference)) {
                    element.assoc(AttributeKey.SOFT_REFERENCE,softReference);
                }
            }
        }
        return element;
    }

    /**
     * Merges the page numbers of the synonyms with the parent element. The merging must take into account any set
     * operations defined on the synonyms.
     *
     * The page numbers of the parent element are always leading, that is to say, all page numbers are added to or removed
     * from the page numbers of the parent element according to the set operation rules.
     *
     * The following set operations are defined:
     * <ul>
     *     <li> Union: All page numbers from the synonym list are added to the page numbers of the parent element.</li>
     *     <li>Intersection: All page numbers that are present in the synonym's list, but that are _not_ present in
     *     the parent list will be removed from the parent list.</li>
     *     <li>Complement (relative complement, or set difference): All page numbers that are present in the synonym's list are removed
     *     from the parent element's page number list.</li>
     * </ul>
     * @param element The index element with synonyms whose page number lists we need to merge.
     */
    private static void mergeSynonyms(IndexElement element) {
        Set<Integer> pageNumbers = new HashSet<>(element.getPages());
        Set<Integer> intersection = new HashSet<>();
        Set<Integer> complement = new HashSet<>();

        // First we add all unions to the final result, and separate out the intersections and complements.
        for (IndexElement synonym : element.getSynonyms()) {
            switch (synonym.getSetOperatorDirective()) {
                case SET_UNION: pageNumbers.addAll(synonym.getPages()); break;
                case SET_INTERSECTION: intersection.addAll(synonym.getPages()); break;
                case SET_COMPLEMENT: complement.addAll(synonym.getPages()); break;
                default: throw new IllegalStateException();
            }
        }
        // Then, if there are any page number sets marked with the intersection operator, we remove all page numbers not
        // present in the intersection set, to get the intersection of `pageNumbers` and `intersection`.
        if (Checks.isNotNullOrEmptyCollection(intersection)) {
            pageNumbers.removeIf(i -> !intersection.contains(i));
        }
        // finally, if there are any page number sets marked with the complement operator, we remove all page numbers
        // that are present in the complement set, to get the relative complement of `complement` in `pageNumbers`.
        if (Checks.isNotNullOrEmptyCollection(complement)) {
            pageNumbers.removeIf(complement::contains);
        }

        Comparator<Integer> comparator = Comparator.naturalOrder();
        ArrayList<Integer> sortedPageNumbers = new ArrayList<>(pageNumbers);
        sortedPageNumbers.sort(comparator);

        // Clear out the children and the old page number list, and put in the new merged and sorted list.
        element.getSynonyms().clear();
        element.getPages().clear();
        element.getPages().addAll(sortedPageNumbers);
    }
}
