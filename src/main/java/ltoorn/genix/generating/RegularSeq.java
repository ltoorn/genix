/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

public class RegularSeq implements PageNumberSeq {

    private final int start;
    private final int end;
    private final boolean isSingleton;

    private RegularSeq(int start, int end, boolean isSingleton) {
        this.start = start;
        this.end = end;
        this.isSingleton = isSingleton;
    }

    /**
     * Constructs a {@code RegularSeq} of the provided {@code start} and {@code end} values.
     * @param start The first value of the sequence.
     * @param end The last value of the sequence.
     * @return The new {@code RegularSeq}.
     */
    public static RegularSeq of(int start, int end) {
        return new RegularSeq(start, end, false);
    }

    /**
     * Constructs a singleton {@code RegularSeq} of the provided {@code val}.
     * @param val The value of the singleton sequence.
     * @return The new singleton {@code RegularSeq}.
     */
    public static RegularSeq singleton(int val) {
        return new RegularSeq(val, val, true);
    }

    @Override
    public String printStart() {
        return String.valueOf(start);
    }

    @Override
    public String printEnd() {
        return String.valueOf(end);
    }

    @Override
    public boolean isSingleton() {
        return isSingleton;
    }

    /**
     * Converts this {@code RegularSeq} into a {@link RomanSeq} by constructing a new {@link RomanSeq}
     * based on the internal {@link this#start} adn {@link this#end} values.
     * @return The new {@link RomanSeq}.
     */
    public RomanSeq toRoman() {
        return isSingleton ? RomanSeq.singleton(start) : RomanSeq.of(start, end);
    }
}
