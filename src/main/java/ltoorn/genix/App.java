/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix;

import ltoorn.genix.context.Context;
import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.dsl.configuration.validation.ConfigurationValidator;
import ltoorn.genix.dsl.shared.ValidationResult;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.dsl.specification.validation.SpecificationValidator;
import ltoorn.genix.generating.IndexGenerator;
import ltoorn.genix.io.Input;
import ltoorn.genix.io.Output;
import ltoorn.genix.io.scraping.Scraper;
import ltoorn.genix.searching.SearchIndexElements;
import ltoorn.genix.util.Checks;

import java.io.File;
import java.util.List;

public class App {

    private static class CommandLineArgs {
        String pdfTextPath       = null;
        String specificationPath = null;
        String configurationPath = null;
    }

    public static void main(String[] args) {

        CommandLineArgs commandLineArgs = parseCommandLineArgs(args);
        if (commandLineArgs == null) {
            return;
        } else if (commandLineArgs.specificationPath == null) {
            scrapeAndPrintListOfWords(commandLineArgs);
            return;
        } else if (!initializeContextWithConfigurationIfPresent(commandLineArgs)) {
            return;
        }

        ValidationResult<List<IndexElement>> validationResult = readAndValidateSpecification(commandLineArgs);
        if (validationResult != null) {
            if (validationResult.isSuccessful()) {
                validationResult.getWarnings().forEach(w -> Output.printMessage(Output.MessageType.WARNING, w.printWithLineNumber()));
                processValidSpecification(commandLineArgs, validationResult);
            } else {
                validationResult.getErrors().forEach(e -> Output.printMessage(Output.MessageType.ERROR, e.printWithLineNumber()));
            }
        }
    }

    private static CommandLineArgs parseCommandLineArgs(String[] args) {
        CommandLineArgs cla = new CommandLineArgs();
        if (args.length < 1 || args.length > 3) {
            Output.printMessage(Output.MessageType.INFO, "Usage: genix <path to PDF file> <optional path to .genix spec> <optional path to config>" +
                                                   "\n\tNote that if no index specification file is provided, Genix will produce an lexicographically" +
                                                   "\n\tordered list of all unique single words encountered in the file");
            return null;
        }

        cla.pdfTextPath = stripQuotesIfNecessary(args[0]);
        if (cla.pdfTextPath == null || !cla.pdfTextPath.endsWith(".pdf")) {
            Output.printMessage(Output.MessageType.ERROR, "The first argument should be a pdf file.");
            return null;
        }

        if (args.length > 1) {
            cla.specificationPath = stripQuotesIfNecessary(args[1]);
            if (cla.specificationPath == null || !cla.specificationPath.endsWith(".genix")) {
                Output.printMessage(Output.MessageType.ERROR, "The second argument should be a genix spec file.");
                return null;
            }
        }

        if (args.length == 3) {
            cla.configurationPath = stripQuotesIfNecessary(args[2]);
            if (cla.configurationPath == null || !cla.configurationPath.endsWith("genix.config")) {
                Output.printMessage(Output.MessageType.ERROR, "The optional third argument should be a genix.config file.");
                return null;
            }
        }
        return cla;
    }

    private static void scrapeAndPrintListOfWords(CommandLineArgs commandLineArgs) {
        File pdfFile = new File(commandLineArgs.pdfTextPath);
        String pdfText = Scraper.getFullPdfText(pdfFile);
        if (Checks.isNotNullOrBlankString(pdfText)) {
            List<String> wordList = Scraper.extractListOfWords(pdfText);
            StringBuilder builder = new StringBuilder();
            for (String word : wordList) {
                builder.append("\"").append(word).append("\"").append("\n");
            }
            String filenamePrefix = pdfFile.getName().split("\\.")[0];
            outputWordList(builder.toString(), filenamePrefix);
        }
    }

    private static boolean initializeContextWithConfigurationIfPresent(CommandLineArgs commandLineArgs) {
        boolean result = false;
        if (commandLineArgs.configurationPath != null) {
            String configuration = Input.readFileToString(commandLineArgs.configurationPath);
            if (Checks.isNotNull(configuration)) {
                ValidationResult<UserConfig> validationResult = ConfigurationValidator.validate(configuration);
                if (validationResult.isSuccessful()) {
                    Context.initialize(validationResult.getResult());
                    result = true;
                } else {
                    validationResult.getErrors().forEach(e -> Output.printMessage(Output.MessageType.ERROR, e.printWithLineNumber()));
                }
            }
        } else {
            Output.printMessage(Output.MessageType.WARNING, "No config file provided. Genix will use default values for all configuration options.");
            Context.initialize(UserConfig.defaultUserConfig());
            result = true;
        }
        return result;
    }

    private static ValidationResult<List<IndexElement>> readAndValidateSpecification(CommandLineArgs commandLineArgs) {
        final String specification = Input.readFileToString(commandLineArgs.specificationPath);
        if (Checks.isNotNullOrBlankString(specification)) {
            return SpecificationValidator.validate(specification);
        } else {
            return null;
        }
    }

    private static void processValidSpecification(CommandLineArgs commandLineArgs, ValidationResult<List<IndexElement>> validationResult) {
        final File pdfFile = new File(commandLineArgs.pdfTextPath);

        Output.printMessage(Output.MessageType.INFO, "Extracting pages from pdf file.");
        final List<String> rawPages = Scraper.getTextPerPdfPage(pdfFile);
        if (Checks.isNotNullOrEmptyCollection(rawPages)) {
            Output.printMessage(Output.MessageType.INFO, "Pre-processing pages.");
            final List<String> pages = Scraper.preProcessRawPages(rawPages);

            Output.printMessage(Output.MessageType.INFO, "Finding occurrences in text.");
            final List<IndexElement> elements = SearchIndexElements.findOccurrences(validationResult.getResult(), pages);

            Output.printMessage(Output.MessageType.INFO, "Generating index.");
            final String index = IndexGenerator.generateIndex(elements);
            final String filenamePrefix = pdfFile.getName().split("\\.")[0];
            outputIndex(index, filenamePrefix);
        } else {
            Output.printMessage(Output.MessageType.ERROR, "There are no pages to index. Please check the PDF and the configuration.");
        }
    }

    private static void outputIndex(String index, String filenamePrefix) {
        Output.outputContent(index, filenamePrefix, "index");
    }

    private static void outputWordList(String wordList, String filenamePrefix) {
        Output.outputContent(wordList, filenamePrefix, "list of words");
    }

    private static String stripQuotesIfNecessary(String s) {
        if (Checks.isNotNullOrBlankString(s)) {
            char first = s.charAt(0);
            char last = s.charAt(s.length()-1);
            if ((first == '\'' && last == '\'') || (first == '"' && last == '"')) {
                String stripped = s.substring(1, s.length()-1);
                return Checks.isNotNullOrBlankString(stripped) ? stripped : null;
            } else {
                return s;
            }
        } else {
            return null;
        }
    }
}
