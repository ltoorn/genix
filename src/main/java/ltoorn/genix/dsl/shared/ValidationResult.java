/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.shared;

import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Contracts;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult<T> {

    private final T result;
    private final List<ValidationMessage> validationErrors;
    private final List<ValidationMessage> validationWarnings;

    private ValidationResult(T result) {
        this.result = result;

        validationErrors = new ArrayList<>();
        validationWarnings = new ArrayList<>();
    }

    public static <T> ValidationResult<T> fromParseResult(IParseResult<T> parseResult) {
        ValidationResult<T> validationResult = new ValidationResult<>(parseResult.getResult());
        for (RecognitionError recognitionError : parseResult.getRecognitionErrors()) {
            validationResult.addError(ValidationMessage.fromRecognitionError(recognitionError));
        }
        return validationResult;
    }

    public T getResult() {
        return result;
    }

    public boolean isSuccessful() {
        return validationErrors.isEmpty();
    }

    public void addError(ValidationMessage validationMessage) {
        validationErrors.add(Contracts.require(validationMessage, Checks::isNotNull));
    }

    public List<ValidationMessage> getErrors() {
        return validationErrors;
    }

    public void addWarning(ValidationMessage validationMessage) {
        validationWarnings.add(Contracts.require(validationMessage, Checks::isNotNull));
    }

    public List<ValidationMessage> getWarnings() {
        return validationWarnings;
    }
}
