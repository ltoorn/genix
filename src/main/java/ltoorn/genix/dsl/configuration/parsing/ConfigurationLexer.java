/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.parsing;

import ltoorn.genix.dsl.shared.LexerUtil;
import ltoorn.genix.dsl.shared.RecognitionError;

public class ConfigurationLexer {

    private final String TRUE = "true";
    private final String FALSE = "false";

    private final ParseResult parseResult;

    private final int[] chars;
    private final int length;

    private int la;
    private int currentLine;

    public ConfigurationLexer(String input, ParseResult parseResult) {

        this.parseResult = parseResult;

        chars = input.codePoints().toArray();
        length = chars.length;

        la = 0;
        currentLine = 1;
    }

    /**
     * Tokenizes the input piecemeal. On each call it consumes until it matches the next full token
     * (which may be the {@link Token.Type#LEXER_ERROR} token on erroneous input).
     * @return The matched {@link Token}.
     */
    public Token nextToken() {

        while (la < length) {
            consumeWhitespace();
            if (la < length && chars[la] == '/') {
                ++la;
                if (la >= length) return reportErrorToken(LexerUtil.unexpectedEndOfFileError(currentLine));
                if (chars[la] == '/') {
                    consumeLineComment();
                } else {
                    return reportErrorToken(LexerUtil.unexpectedInputError(chars[la], '/', currentLine));
                }
            } else {
                break;

            }
        }

        if (la < length) {
            switch (chars[la]) {
                case 't':
                case 'f':
                    if (predictBoolean()) {
                        return matchBoolean();
                    }
                    break;

                case '"': return matchString();
                case '=': ++la; return new Token(Token.Type.EQ, "=", currentLine);
                case '(': ++la; return new Token(Token.Type.OPEN_PAREN, "(", currentLine);
                case ')': ++la; return new Token(Token.Type.CLOSE_PAREN, ")", currentLine);
                case ',': ++la; return new Token(Token.Type.COMMA, ",", currentLine);
            }

            if (chars[la] >= '0' && chars[la] <= '9') {
                return matchInteger();
            } else if (LexerUtil.isPlainAsciiLetter(chars[la])) {
                return matchOptionName();
            } else {
                StringBuilder errorMessage = new StringBuilder("Invalid input: '")
                        .appendCodePoint(chars[la])
                        .append("'.");
                return reportErrorToken(new RecognitionError(currentLine, errorMessage.toString()));
            }
        }
        return new Token(Token.Type.EOF, "<end of input>", currentLine);
    }

    private boolean predictBoolean() {
        int la = this.la;
        final String toMatch;
        if (chars[la] == 't') {
            toMatch = TRUE;
        } else {
            toMatch = FALSE;
        }
        for (int i = 0; i < toMatch.length(); i++) {
            if (la >= length) {
                return false;
            }
            if (chars[la++] != toMatch.codePointAt(i)) {
                return false;
            }
        }
        return la >= length || Character.isWhitespace(chars[la]);
    }

    /**
     * Matches a boolean configuration value:
     *
     *     BOOLEAN ::= 'true' | 'false'
     *
     * @return The matched boolean type {@link Token}.
     */
    private Token matchBoolean() {
        final String val;
        if (chars[la] == 't') {
            la += TRUE.length();
            val = TRUE;
        } else {
            la += FALSE.length();
            val = FALSE;
        }
        return new Token(Token.Type.BOOLEAN, val, currentLine);
    }

    /**
     * Matches a string configuration value:
     *
     *     STRING ::= '"' (~'"' | '\\' '"')+ '"'
     *
     * @return The matched string type {@link Token}.
     */
    private Token matchString() {
        ++la; // Move past the opening quote '"'.
        RecognitionError error;
        StringBuilder matchedString = new StringBuilder();
        for (;;) {
            if (la >= length) {
                return reportErrorToken(LexerUtil.unexpectedEndOfFileError(currentLine));
            }
            if (chars[la] == '"') {
                ++la;
                return new Token(Token.Type.STRING, matchedString.toString(), currentLine);
            } else if (chars[la] == '\\') {
                ++la;
                if (la >= length) {
                    return reportErrorToken(LexerUtil.unexpectedEndOfFileError(currentLine));
                }
                if (chars[la] == 'n') {
                    matchedString.append("\n");
                    ++la;
                    continue;
                } else if (chars[la] == 't') {
                    matchedString.append("\t");
                    ++la;
                    continue;
                } else if (chars[la] != '"' && chars[la] != '\\') {
                    error = new RecognitionError(currentLine, "Illegal escape sequence.");
                    break;
                }
            } else if (chars[la] == '\n' || chars[la] == '\r') {
                consumeNewLine();
                error = new RecognitionError(currentLine, "String literals may not span multiple lines. If you wish to incorporate a newline into the text you should use the newline escape sequence: '\\n'.");
                break;
            }
            matchedString.appendCodePoint(chars[la++]);
        }
        while (chars[la] != '"') {
            if (la++ >= length) {
                return reportErrorToken(LexerUtil.unexpectedEndOfFileError(currentLine));
            }
        }
        return reportErrorToken(error);
    }

    /**
     * Matches an integer configuration value:
     *
     *     INTEGER ::= [0-9]+
     *
     * @return The matched integer type {@link Token}.
     */
    private Token matchInteger() {
        StringBuilder result = new StringBuilder();
        while (la < length && chars[la] >= '0' && chars[la] <= '9') {
            result.appendCodePoint(chars[la++]);
        }
        if (la < length && chars[la] != ',' && chars[la] != ')' && !Character.isWhitespace(chars[la])) {
            return reportErrorToken(new RecognitionError(currentLine, "Ill formed integer."));
        }
        return new Token(Token.Type.INTEGER, result.toString(), currentLine);
    }

    /**
     * Matches the name of a configuration option:
     *
     *     OPTION_NAME ::= [a-zA-Z]+
     *
     * @return The matched option name {@link Token}.
     */
    private Token matchOptionName() {
        StringBuilder result = new StringBuilder();
        while (la < length && (LexerUtil.isPlainAsciiLetter(chars[la]))) {
            result.appendCodePoint(chars[la++]);
        }
        if (la < length && !Character.isWhitespace(chars[la])) {
            return reportErrorToken(new RecognitionError(currentLine, "Invalid configuration option"));
        }
        return new Token(Token.Type.OPTION_NAME, result.toString(), currentLine);
    }

    private void consumeWhitespace() {
        while (la < length && Character.isWhitespace(chars[la])) {
            if (chars[la] == '\n' || chars[la] == '\r') {
                consumeNewLine();
                continue;
            }
            ++la;
        }
    }

    private void consumeNewLine() {
        if (chars[la] == '\r') {
            ++la;
            if (la < length && chars[la] == '\n') {
                ++la;
            }
        } else if (chars[la] == '\n') {
            ++la;
        }
        ++currentLine;
    }

    private void consumeLineComment() {
        while (la < length) {
            if (chars[la] == '\n' || chars[la] == '\r') {
                consumeNewLine();
                return;
            }
            ++la;
        }
    }

    private Token reportErrorToken(RecognitionError error) {
        parseResult.addRecognitionError(error);
        ++la;
        return new Token(Token.Type.LEXER_ERROR, "", currentLine);
    }
}
