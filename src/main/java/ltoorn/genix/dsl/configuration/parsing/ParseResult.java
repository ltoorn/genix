/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.parsing;

import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.dsl.configuration.ir.ConfigOptionValueResult;
import ltoorn.genix.dsl.shared.IParseResult;
import ltoorn.genix.dsl.shared.RecognitionError;

import java.util.ArrayList;
import java.util.List;

public class ParseResult implements IParseResult<UserConfig> {

    private final UserConfig userConfig;
    private final List<ConfigOptionValueResult> configOptions;
    private final List<RecognitionError> recognitionErrors;

    public ParseResult() {
        userConfig = new UserConfig();
        configOptions = new ArrayList<>();
        recognitionErrors = new ArrayList<>();
    }

    @Override
    public UserConfig getResult() {
        return userConfig;
    }

    public void addConfigOption(ConfigOptionValueResult configOption) {
        configOptions.add(configOption);
    }

    public List<ConfigOptionValueResult> getConfigOptions() {
        return configOptions;
    }

    public void addRecognitionError(RecognitionError error) {
        recognitionErrors.add(error);
    }

    @Override
    public List<RecognitionError> getRecognitionErrors() {
        return recognitionErrors;
    }
}
