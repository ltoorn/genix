/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.parsing;

import ltoorn.genix.context.configuration.ConfigOption;
import ltoorn.genix.dsl.configuration.ir.ConfigOptionValueResult;
import ltoorn.genix.dsl.shared.CancelParseException;
import ltoorn.genix.dsl.shared.RecognitionError;
import ltoorn.genix.util.Pair;

public class ConfigurationParser {

    private final ParseResult parseResult;
    private final ConfigurationLexer lexer;

    private Token la;

    public ConfigurationParser(String input) {
        parseResult = new ParseResult();
        lexer = new ConfigurationLexer(input, parseResult);
    }

    /**
     * Parses the configuration file 'config':
     *
     *     config ::= configOption* EOF
     *
     * The parser consumes the token stream produced by the {@link ConfigurationLexer} into a {@link ParseResult}.
     *
     * Any encountered errors will be contained in the parse result. If we encounter a lexer error, we
     * abort the parse, but we do exhaust the token stream before returning, to present all lexer errors to
     * the user at once. When we encounter parse errors, we continue parsing after the error, reporting all
     * error we encounter along the way.
     *
     * @return The parse result.
     */
    public ParseResult parse() {

        try {
            nextToken();

            while (la.type != Token.Type.EOF) {
                if (la.type == Token.Type.OPTION_NAME) {

                }
                if (la.type == Token.Type.OPTION_NAME) {
                    ConfigOptionValueResult configOption = matchConfigOption();
                    if (configOption != null) {
                        parseResult.addConfigOption(configOption);
                    }
                } else {
                    reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.OPTION_NAME, la));
                }
            }
        } catch (CancelParseException __) {
            while (lexer.nextToken().type != Token.Type.EOF);
        }

        return parseResult;
    }

    /**
     * Matches a config option:
     *
     *     configOption ::= OPTION_NAME '=' configValue
     *
     * @return The tokenized config option.
     */
    private ConfigOptionValueResult matchConfigOption() {
        ConfigOptionValueResult configOptionValueResult = new ConfigOptionValueResult();

        configOptionValueResult.configOption = ConfigOption.fromName(la.text);
        configOptionValueResult.configOptionLineNr = la.line;

        if (configOptionValueResult.configOption == null) {
            reportError(la.line, "Unknown configuration option: '" + la.text + "'.");
            return null;
        }
        nextToken();

        if (la.type != Token.Type.EQ) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.EQ, la));
            return null;
        }
        nextToken();

        Pair<ConfigOption.ValueType, Object> typeValuePair = matchConfigValue();
        if (typeValuePair == null) {
            return null;
        }
        configOptionValueResult.typeValuePair = typeValuePair;

        return configOptionValueResult;
    }

    /**
     * Matches a config value:
     *
     *     configValue ::= pair | BOOLEAN | INTEGER | STRING
     *
     * @return The tokenized config value.
     */
    private Pair<ConfigOption.ValueType, Object> matchConfigValue() {
        ConfigOption.ValueType type = null;
        Object result = null;
        switch (la.type) {
            case STRING:
                type = ConfigOption.ValueType.STRING;
                result = la.text;
                break;
            case INTEGER:
                type = ConfigOption.ValueType.INTEGER;
                // Our lexer is responsible for guaranteeing that a NumberFormatException cannot occur here.
                result = Integer.parseInt(la.text);
                break;
            case BOOLEAN:
                type = ConfigOption.ValueType.BOOLEAN;
                result = Boolean.parseBoolean(la.text);
                break;
            case OPEN_PAREN:
                type = ConfigOption.ValueType.PAIR;
                result = matchPairValue();
                break;
            case EOF:
                reportError(la.line, "Unexpected end of input.");
                return null;
            default:
                String errorMessage =
                        buildUnexpectedTokenMultipleErrorMessage(
                                new Token.Type[]{Token.Type.STRING, Token.Type.INTEGER, Token.Type.BOOLEAN, Token.Type.OPEN_PAREN},
                                la
                        );
                reportError(la.line, errorMessage);
                return null;
        }
        nextToken();
        if (result == null) {
            return null;
        }
        return Pair.of(type, result);
    }

    /**
     * Matches a pair value:
     *
     *     pair ::= '(' INTEGER ',' INTEGER ')'
     *
     * @return The tokenized pair value.
     */
    private Pair<Integer, Integer> matchPairValue() {
        nextToken(); // Move past the open paren '('.
        if (la.type != Token.Type.INTEGER) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.INTEGER, la));
            return null;
        }

        Integer left = Integer.parseInt(la.text);
        nextToken();

        if (la.type != Token.Type.COMMA) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.COMMA, la));
            return null;
        }
        nextToken();

        if (la.type != Token.Type.INTEGER) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.INTEGER, la));
            return null;
        }

        Integer right = Integer.parseInt(la.text);
        nextToken();

        if (la.type != Token.Type.CLOSE_PAREN) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.CLOSE_PAREN, la));
            return null;
        }
        // The closing paren ')' will be consumed by the calling method.
        return Pair.of(left, right);
    }

    private void nextToken() {
        la = lexer.nextToken();
        if (la.type == Token.Type.LEXER_ERROR) {
            throw new CancelParseException();
        }
    }

    private String buildUnexpectedTokenErrorMessage(Token.Type expectedType, Token providedToken) {
        StringBuilder message = new StringBuilder("Unexpected token. Expected a token of type: '")
                .append(expectedType.name())
                .append("', but received a token of type: '")
                .append(providedToken.type)
                .append("'. (Token content: '")
                .append(providedToken.text)
                .append("'.)");

        return message.toString();
    }

    private String buildUnexpectedTokenMultipleErrorMessage(Token.Type[] possibleTypes, Token providedToken) {
        StringBuilder message = new StringBuilder("Unexpected token. Expected a token of one of the following types:\n");

        for (Token.Type expected : possibleTypes) {
            message.append("'").append(expected.name()).append("', ");
        }

        message.append("\nbut received a token of type: '")
                .append(providedToken.type.name())
                .append("'. (Token content: '")
                .append(providedToken.text)
                .append("'.)");

        return message.toString();
    }

    private void reportError(int line, String errorMessage) {
        parseResult.addRecognitionError(new RecognitionError(line, errorMessage));
        nextToken();
    }
}
