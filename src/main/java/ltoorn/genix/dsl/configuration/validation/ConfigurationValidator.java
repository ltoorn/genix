/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.validation;

import ltoorn.genix.context.configuration.ConfigOption;
import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.dsl.configuration.ir.ConfigOptionValueResult;
import ltoorn.genix.dsl.configuration.parsing.ConfigurationParser;
import ltoorn.genix.dsl.configuration.parsing.ParseResult;
import ltoorn.genix.dsl.shared.ValidationMessage;
import ltoorn.genix.dsl.shared.ValidationResult;
import ltoorn.genix.util.Pair;

import java.util.List;

public final class ConfigurationValidator {

    private ConfigurationValidator() {}

    public static ValidationResult<UserConfig> validate(String configuration) {
        ConfigurationParser parser = new ConfigurationParser(configuration);
        ParseResult parseResult = parser.parse();
        ValidationResult<UserConfig> validationResult = ValidationResult.fromParseResult(parseResult);

        typeCheck(validationResult, parseResult.getConfigOptions());

        return validationResult;
    }

    private static void typeCheck(ValidationResult<UserConfig> result, List<ConfigOptionValueResult> configOptions) {

        for (ConfigOptionValueResult configOptionValueResult : configOptions) {

            ConfigOption configOption = configOptionValueResult.configOption;
            Pair<ConfigOption.ValueType, Object> typeValuePair = configOptionValueResult.typeValuePair;

            ConfigOption.ValueType expectedType = ConfigOption.getValueTypeForOption(configOption);
            ConfigOption.ValueType actualType = typeValuePair.getLeft();

            if (expectedType == actualType) {
                result.getResult().addOption(configOption, typeValuePair.getRight());
            } else {
                String message =
                        "Type mismatch in configuration file. Expected: '"
                                + expectedType.name()
                                + "', got: '"
                                + actualType.name()
                                + "'.";

                result.addError(new ValidationMessage(message, configOptionValueResult.configOptionLineNr));
            }
        }
    }
}
