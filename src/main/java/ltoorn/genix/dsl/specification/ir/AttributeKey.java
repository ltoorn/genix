/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.ir;

import ltoorn.genix.dsl.specification.parsing.Token;
import ltoorn.genix.util.Checks;

// todo: documentation
public enum AttributeKey {

    HARD_REFERENCE("hardReference"),
    SOFT_REFERENCE("softReference"),
    MEMBERS("members"),
    INDEXABLE("indexable"),

    POSITION_DIRECTIVE(""),
    PAGE_NUMBERS(""),
    SYNONYMS(""),
    SET_OPERATOR(""),

    UNKNOWN("<unknown>");

    public final String text;

    AttributeKey(String text) {
        this.text = text;
    }

    public static AttributeKey fromText(String attributeText) {
        for (AttributeKey key : AttributeKey.values()) {
            if (Checks.isNotNullOrBlankString(key.text) && key.text.equalsIgnoreCase(attributeText)) {
                return key;
            }
        }
        return UNKNOWN;
    }

    public static boolean isTokenTypeAllowedForKey(AttributeKey key, Token.Type type) {
        if (key == UNKNOWN) {
            // We allow any type in combination with an UNKNOWN key, because the UNKNOWN key is already an error
            // in itself. Adding a type error will be of no use, and will only result in confusing error messages.
            return true;
        }
        return expectedTokenTypeFor(key) == type;
    }

    public static Token.Type expectedTokenTypeFor(AttributeKey key) {
        switch (key) {
            case HARD_REFERENCE:
            case SOFT_REFERENCE:
                return Token.Type.LITERAL;

            case MEMBERS:
                return Token.Type.OPEN_PAREN;

            case INDEXABLE:
                return Token.Type.BOOLEAN_VALUE;

            default:
                return null;
        }
    }
}
