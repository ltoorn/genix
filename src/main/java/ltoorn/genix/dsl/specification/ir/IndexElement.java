/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.ir;

import ltoorn.genix.dsl.specification.parsing.Token;
import ltoorn.genix.searching.SearchUtil;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Contracts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

/**
 * Defines the internal representation for all index elements as they are extracted from the index specification
 * and used by the rest of the program.
 */
@SuppressWarnings("unchecked")
public class IndexElement {

    public final String contentForSearch;
    public final String contentForOutput;
    public final int lineNumber;

    private final HashMap<AttributeKey, Object> attributes;

    private IndexElement(Token token) {
        contentForSearch = SearchUtil.stripHyphens(token.text);
        contentForOutput = token.text;

        this.lineNumber = token.line;

        attributes = new HashMap<>();

        assoc(AttributeKey.PAGE_NUMBERS, new ArrayList<Integer>());
    }

    public static IndexElement of(Token token) {
        return new IndexElement(token);
    }

    public void addMember(IndexElement member) {
        addToList(AttributeKey.MEMBERS, member);
    }

    public ArrayList<IndexElement> getMembers() {
        return getChildren(AttributeKey.MEMBERS);
    }

    public void addSynonym(IndexElement synonym) {
        addToList(AttributeKey.SYNONYMS, synonym);
    }

    public ArrayList<IndexElement> getSynonyms() {
        return getChildren(AttributeKey.SYNONYMS);
    }

    public void addPage(Integer i) {
        Contracts.assume(isIndexable());
        ArrayList<Integer> pageNumbers = (ArrayList<Integer>) get(AttributeKey.PAGE_NUMBERS);
        pageNumbers.add(i);
    }

    public ArrayList<Integer> getPages() {
        Contracts.assume(isIndexable());
        return (ArrayList<Integer>) attributes.get(AttributeKey.PAGE_NUMBERS);
    }

    /**
     * Determines whether the parent is indexable. When the attribute hasn't been specified by the
     * user, the following rule applies:
     * <em>if</em> the element has a hard reference we default to {@code false},
     * <em>otherwise</em> we default to {@code true}.
     * @return {@code true} if the element is indexable, {@code false} otherwise.
     */
    public boolean isIndexable() {
        if (hasHardReference() && getNullable(AttributeKey.INDEXABLE) == null) {
            return false;
        } else {
            return !Boolean.FALSE.equals(getNullable(AttributeKey.INDEXABLE));
        }
    }

    public IndexElement getHardReference() {
        return (IndexElement) get(AttributeKey.HARD_REFERENCE);
    }

    public List<IndexElement> getSoftReferences() {
        Contracts.assume(hasSoftReference());
        return (List<IndexElement>) get(AttributeKey.SOFT_REFERENCE);
    }

    public void assocPositionDirective(Directive directive) {
        assoc(AttributeKey.POSITION_DIRECTIVE, directive);
    }

    public Directive getPositionDirective() {
        return (Directive) get(AttributeKey.POSITION_DIRECTIVE);
    }

    public void assocSetOperatorDirective(Directive setOperator) {
        assoc(AttributeKey.SET_OPERATOR, setOperator);
    }

    public Directive getSetOperatorDirective() {
        return (Directive) get(AttributeKey.SET_OPERATOR);
    }

    public boolean hasSynonyms() {
        return Checks.isNotNullOrEmptyCollection(getSynonyms());
    }

    public boolean hasMembers() {
        return Checks.isNotNullOrEmptyCollection(getMembers());
    }

    public boolean hasHardReference() {
        return Checks.isNotNull(getNullable(AttributeKey.HARD_REFERENCE));
    }

    public boolean hasSoftReference() {
        return Checks.isNotNullOrEmptyCollection((List<IndexElement>) getNullable(AttributeKey.SOFT_REFERENCE));
    }

    public void assoc(AttributeKey key, Object attribute) {
        if (key == AttributeKey.SOFT_REFERENCE) {
            Contracts.assume(attribute instanceof IndexElement);
            addToList(key, (IndexElement) attribute);
        } else {
            Contracts.assume(!attributes.containsKey(key), "Overwriting a key ('" + key.name() + "') is not allowed");
            attributes.put(key, attribute);
        }
    }

    public void dissoc(AttributeKey key) {
        attributes.remove(key);
    }

    public boolean isAssociatedKey(AttributeKey key) {
        return attributes.containsKey(key);
    }

    private <T> void addToList(AttributeKey key, T listElement) {
        List<T> list = (List<T>) attributes.computeIfAbsent(key, k -> new ArrayList<T>());
        list.add(listElement);
    }

    private ArrayList<IndexElement> getChildren(AttributeKey key) {
        Contracts.assumeAny(key == AttributeKey.MEMBERS, key == AttributeKey.SYNONYMS);
        return (ArrayList<IndexElement>) attributes.get(key);
    }

    private Object get(AttributeKey key) {
        Contracts.assume(attributes.containsKey(key), "Can't get non-existent key: '" + key.name() + "'.");
        return attributes.get(key);
    }

    private Object getNullable(AttributeKey key) {
        return attributes.get(key);
    }
}
