/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.ir;

import ltoorn.genix.util.Checks;

public enum Directive {

    PARENT("parent"),
    INDEXABLE("indexable"),
    LEFT("left"),
    RIGHT("right"),
    LEFT_OR_RIGHT("leftOrRight"),
    SET_UNION("union"),
    SET_INTERSECTION("intersection"),
    SET_COMPLEMENT("complement"),

    // Internal directive, that marks the absence of a directive, which is semantically significant for position
    // directives, and semantically different from UNKNOWN, which marks a directive entered by the user that is
    // not one of the pre-defined directives the program recognizes, and thus always indicates an error.
    NONE("<none>"),

    UNKNOWN("<unknown>");

    public final String text;

    Directive(String text) {
        this.text = text;
    }

    public static Directive fromString(String s) {
        for (Directive directive : Directive.values()) {
            if (Checks.isNotNullOrBlankString(directive.text) && directive.text.equalsIgnoreCase(s)) {
                return directive;
            }
        }
        return UNKNOWN;
    }

    public boolean isPositionDirective() {
        switch (this) {
            case LEFT:
            case RIGHT:
            case LEFT_OR_RIGHT:
                return true;
            default:
                return false;
        }
    }

    public boolean isSetOperatorDirective() {
        switch (this) {
            case SET_UNION:
            case SET_INTERSECTION:
            case SET_COMPLEMENT:
                return true;
            default:
                return false;
        }
    }
}
