/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.parsing;

import ltoorn.genix.dsl.shared.IParseResult;
import ltoorn.genix.dsl.shared.RecognitionError;
import ltoorn.genix.dsl.specification.ir.AttributeKey;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class ParseResult implements IParseResult<List<IndexElement>> {

    private final ArrayList<IndexElement> parsedElements;
    private final ArrayList<Pair<IndexElement, Pair<AttributeKey, String>>> references;
    private final ArrayList<RecognitionError> recognitionErrors;

    private int lineNrOfLastAddedError;

    public ParseResult() {
        parsedElements = new ArrayList<>();
        references = new ArrayList<>();
        recognitionErrors = new ArrayList<>();

        lineNrOfLastAddedError = -1;
    }

    public void addIndexElement(IndexElement element) {
        parsedElements.add(element);
    }

    public void addReference(IndexElement element, Pair<AttributeKey, String> reference) {
        references.add(Pair.of(element, reference));
    }

    public ArrayList<Pair<IndexElement, Pair<AttributeKey, String>>> getReferences() {
        return references;
    }

    public void addRecognitionError(RecognitionError error) {
        lineNrOfLastAddedError = error.line;
        recognitionErrors.add(error);
    }

    @Override
    public ArrayList<RecognitionError> getRecognitionErrors() {
        return recognitionErrors;
    }

    @Override
    public ArrayList<IndexElement> getResult() {
        return parsedElements;
    }

    public int getLineNrOfLastAddedError() {
        return lineNrOfLastAddedError;
    }
}
