/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.parsing;

import ltoorn.genix.dsl.shared.LexerUtil;
import ltoorn.genix.dsl.shared.RecognitionError;
import ltoorn.genix.util.Checks;

public class SpecificationLexer {

    private final ParseResult parseResult;

    private final int[] chars;
    private final int length;

    private int lookahead;
    private int currentLine;

    public SpecificationLexer(String input, ParseResult parseResult) {
        this.parseResult = parseResult;

        chars = input.codePoints().toArray();
        length = chars.length;
        lookahead = 0;
        currentLine = 1;
    }

    /**
     * Tokenizes the input piecemeal. On each call we consume until we match the next full token
     * (which may be the {@link Token.Type#LEXER_ERROR} token on erroneous input).
     * @return The matched {@link Token}.
     */
    public Token nextToken() {

        // First consume all whitespace and comments
        while (lookahead < length) {
            consumeWhitespace();
            if (lookahead < length && chars[lookahead] == '/') {
                advance(1);
                if (lookahead >= length) return reportErrorToken(unexpectedEndOfFileError());
                if (chars[lookahead] == '/') {
                    consumeLineComment();
                } else {
                    return reportErrorToken(unexpectedInputError(chars[lookahead], '/', currentLine));
                }
            } else {
                break;
            }
        }

        if (lookahead < length) {

            int currentChar = chars[lookahead];
            switch (currentChar) {

                case '(': return consumeToken(Token.Type.OPEN_PAREN);
                case ')': return consumeToken(Token.Type.CLOSE_PAREN);

                case '{': return consumeToken(Token.Type.OPEN_CURLY);
                case '}': return consumeToken(Token.Type.CLOSE_CURLY);

                case ',': return consumeToken(Token.Type.COMMA);

                case '=': return consumeToken(Token.Type.EQ);

                case '"': return matchLiteral();

                case '@': return matchDirective();

                case ':': return matchAttribute();

                case 'w': return matchKeywordWith();

                case 't':
                case 'f': return matchBooleanValue();

                default: return reportErrorToken(new RecognitionError(currentLine, "Invalid input: '" + codePointToString(currentChar) + "'."));
            }
        }
        return new Token(Token.Type.EOF, Token.Type.EOF.text, currentLine);
    }

    /**
     * Matches a literal token:
     *
     *     LITERAL ::= '"' (~'"' | '\\' '"')+ '"' ;
     *
     * @return The tokenized literal.
     */
    private Token matchLiteral() {
        // We advance one character to go past the opening '"'.
        advance(1);

        if (lookahead < length && chars[lookahead] == '"') {
            return reportErrorToken(new RecognitionError(currentLine, "Empty strings are not allowed."));
        }

        Token errorToken;
        StringBuilder literalContent = new StringBuilder();
        for (;;) {
            if (lookahead >= length) {
                return reportErrorToken(unexpectedEndOfFileError());
            }

            if (chars[lookahead] == '"') {
                // Advance one more character to go past the closing '"'.
                advance(1);
                return new Token(Token.Type.LITERAL, literalContent.toString(), currentLine);
            } else if (chars[lookahead] == '\\') {
                // We have encountered an escape sequence. We allow double quotes to be escaped, and we allow the
                // backslash character itself to be escaped.
                int la1 = lookahead + 1;
                if (la1 >= length) {
                    return reportErrorToken(unexpectedEndOfFileError());
                } else if (chars[la1] == '"' || chars[la1] == '\\') {
                    // If the character following the initial backslash is a double quote or a backslash, then the escape
                    // sequence is legal. We advance past the escape character and fall through to the bottom of the loop
                    // to add the escaped character itself, and advance past it.
                    advance(1);
                } else {
                    errorToken = reportErrorToken(new RecognitionError(currentLine, "Illegal use of escape character: '\\'."));
                    break;
                }
            } else if (chars[lookahead] == '\n' || chars[lookahead] == '\r') {
                consumeNewLine();
                // We don't allow for unescaped newlines within literals.
                errorToken = reportErrorToken(new RecognitionError(currentLine, "Illegal use of line break within a literal."));
                break;
            }
            literalContent.appendCodePoint(chars[lookahead]);
            advance(1);
        }

        // We only get here when we break from the loop above due to an error. We then move to the end of the literal,
        // to allow the lexer to resume after it, and we return the error token.
        while (lookahead < length) {
            if (chars[lookahead] == '"') {
                advance(1);
                break;
            }
            advance(1);
        }
        return errorToken;
    }

    /**
     * Matches a directive token:
     *
     *     DIRECTIVE ::= '@' IDENTIFIER
     *
     * @return The tokenized directive.
     */
    private Token matchDirective() {
        return directiveAndAttributeHelper(Token.Type.DIRECTIVE);
    }

    /**
     * Matches an attribute token:
     *
     *     ATTRIBUTE ::= ':' IDENTIFIER
     *
     * @return The tokenized attribute.
     */
    private Token matchAttribute() {
        return directiveAndAttributeHelper(Token.Type.ATTRIBUTE);
    }

    private Token directiveAndAttributeHelper(Token.Type type) {
        advance(1);
        String identifier = matchIdentifier();
        if (Checks.isNotNullOrBlankString(identifier)) {
            return new Token(type, identifier, currentLine);
        } else {
            return reportErrorToken(new RecognitionError(currentLine, "Empty identifiers are not allowed."));
        }
    }

    /**
     * Matches the keyword with:
     *
     *     KEYWORD_WITH ::= ('w'|'W')('i'|'I')('t'|'T')('h'|'H')
     *
     * @return The tokenized keyword.
     */
    private Token matchKeywordWith() {
        String keyword = matchIdentifier();
        if ("with".equalsIgnoreCase(keyword)) {
            return new Token(Token.Type.KEYWORD_WITH, keyword, currentLine);
        } else {
            return reportErrorToken(new RecognitionError(currentLine, "Unexpected keyword: '" + keyword + "'."));
        }
    }

    /**
     * Matches a boolean value literal:
     *
     *    KEYWORD_TRUE ::= ('t'|'T')('r'|'R')('u'|'U')('e'|'E')
     *    KEYWORD_FALSE ::= ('f'|'F')('a'|'A')('l'|'L')('s'|'S')('e'|'E')
     *
     * @return The tokenized boolean value.
     */
    private Token matchBooleanValue() {
        String keyword = matchIdentifier();
        if ("true".equalsIgnoreCase(keyword) || "false".equalsIgnoreCase(keyword)) {
            return new Token(Token.Type.BOOLEAN_VALUE, keyword, currentLine);
        } else {
            return reportErrorToken(new RecognitionError(currentLine, "Unexpected keyword: '" + keyword + "'."));
        }
    }

    /**
     * Matches an identifier:
     *
     *     IDENTIFIER ::= [a-zA-Z]+
     *
     * Note that the caller is responsible for checking whether the identifier is valid. This method only guarantees
     * that all characters are from the specified character set, but not whether the identifier is non-empty.
     *
     * @return The matched identifier as string
     */
    private String matchIdentifier() {
        StringBuilder identifier = new StringBuilder();
        while (LexerUtil.isPlainAsciiLetter(chars[lookahead])) {
            identifier.appendCodePoint(chars[lookahead]);
            advance(1);
        }
        return identifier.toString();
    }

    private Token consumeToken(Token.Type type) {
        advance(type.text.length());
        return new Token(type, type.text, currentLine);
    }

    private void consumeWhitespace() {
        while (lookahead < length && Character.isWhitespace(chars[lookahead])) {
            if (chars[lookahead] == '\n' || chars[lookahead] == '\r') {
                consumeNewLine();
                continue;
            }
            advance(1);
        }
    }

    private void consumeNewLine() {
        if (chars[lookahead] == '\r') {
            advance(1);
            if (lookahead < length && chars[lookahead] == '\n') {
                advance(1);
            }
        } else if (chars[lookahead] == '\n') {
            advance(1);
        }
        ++currentLine;
    }

    private void consumeLineComment() {
        while (lookahead < length) {
            if (chars[lookahead] == '\n' || chars[lookahead] == '\r') {
                consumeNewLine();
                return;
            }
            advance(1);
        }
    }

    private String codePointToString(int codePoint) {
        return new StringBuilder().appendCodePoint(codePoint).toString();
    }

    private void advance(int step) {
        lookahead += step;
    }

    private Token reportErrorToken(RecognitionError error) {
        parseResult.addRecognitionError(error);
        advance(1);
        return new Token(Token.Type.LEXER_ERROR, "", currentLine);
    }

    private RecognitionError unexpectedInputError(int actual, int expected, int line) {
        return LexerUtil.unexpectedInputError(actual, expected, line);
    }

    private RecognitionError unexpectedEndOfFileError() {
        return LexerUtil.unexpectedEndOfFileError(currentLine);
    }
}
