/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.parsing;

import ltoorn.genix.dsl.shared.CancelParseException;
import ltoorn.genix.dsl.shared.RecognitionError;
import ltoorn.genix.dsl.specification.ir.AttributeKey;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.dsl.specification.ir.Directive;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Pair;

import java.util.function.Consumer;

public class SpecificationParser {

    private final SpecificationLexer lexer;
    private final ParseResult parseResult;

    private Token la;
    private int listOrBlockDelimiterDepth;

    public SpecificationParser(String input) {
        parseResult = new ParseResult();
        lexer = new SpecificationLexer(input, parseResult);

        consume(); // Initialize la token.

        listOrBlockDelimiterDepth = 0;
    }

    /**
     * Parses the specification, building up a parse result object in the process.
     *
     * The specification consists of one or more elements followed by the end of file marker.
     * The full Genix specification syntax is as follows:
     *
     *     specification ::= element+ EOF
     *     element ::= LITERAL synonymsList? attributesBlock?
     *     synonymsList ::= '(' synonym (',' synonym)* ')'
     *     synonym ::= directive? LITERAL
     *     attributesBlock ::= KEYWORD_WITH '{' attribute (',' attribute)* '}'
     *     attribute ::= attributeKey '=' attributeValue
     *     attributeKey ::= ':' IDENTIFIER
     *     attributeValue ::= LITERAL | boolean | memberList
     *     boolean ::= KEYWORD_TRUE | KEYWORD_FALSE
     *     directive ::= '@' IDENTIFIER
     *     memberList ::= '(' member (',' member)* ')'
     *     member ::= directive? element
     *
     * @return The {@link ParseResult}.
     */
    public ParseResult parse() {

        if (la.type == Token.Type.EOF) {
            reportError(la.line, "The specification was empty.");
        }

        try {
            while (la.type != Token.Type.EOF) {
                IndexElement element = matchElement();
                acceptIfNonNull(parseResult::addIndexElement, element);
            }
        } catch (CancelParseException __) {
            parseResult.addRecognitionError(new RecognitionError(la.line, "The specification parser stopped processing the specification file because it encountered too many errors."));
        }

        return parseResult;
    }

    /**
     * Matches an element:
     *
     *     element ::= LITERAL synonymsList? (KEYWORD_WITH '{' attribute+ '}')?
     *
     * @return The matched {@link IndexElement}.
     */
    private IndexElement matchElement() {

        if (la.type != Token.Type.LITERAL) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.LITERAL, la));
            consumeUntilNextTopLevelLiteral();
            return null;
        }

        IndexElement element = IndexElement.of(la);
        consume();

        if (la.type == Token.Type.OPEN_PAREN) {
            int openParenLineNumber = la.line;
            enterBlockOrList();
            if (!matchSynonymsList(element, openParenLineNumber)) {
                return null;
            }
        }

        if (la.type == Token.Type.KEYWORD_WITH) {
            int keywordWithLineNumber = la.line;
            consume();
            if (!matchAttributesBlock(element, keywordWithLineNumber)) {
                consumeUntilNextTopLevelLiteral();
                return null;
            }
        }

        return element;
    }

    /**
     * Matches an attribute block.
     *
     *     attributesBlock ::= KEYWORD_WITH '{' attribute (',' attribute)* '}'
     *
     * @param parent The parent index element whose attributes are defined.
     * @param startOfBlockLineNr The line number of the 'with' keyword.
     * @return {@code true} if the attributes block is matched without errors, {@code false} otherwise.
     */
    private boolean matchAttributesBlock(IndexElement parent, int startOfBlockLineNr) {
        if (la.type != Token.Type.OPEN_CURLY) {
            reportError(la.line, buildUnexpectedTokenErrorMessage(Token.Type.OPEN_CURLY, la));
            return false;
        }
        enterBlockOrList(); // consume '{'

        for (;;) {
            if (!predict(Token.Type.ATTRIBUTE)) {
                return false;
            }

            if (!matchAttribute(parent)) {
                return false;
            }

            switch (la.type) {
                case COMMA:
                    consume();
                    break;
                case CLOSE_CURLY:
                    leaveBlockOrList();
                    return true;
                case EOF:
                    reportEOF("Unclosed attributes block. The start of the block was on line: " + startOfBlockLineNr + ".");
                    return false;
                default:
                    reportError(la.line, buildUnexpectedTokenMultipleErrorMessage(new Token.Type[]{Token.Type.COMMA, Token.Type.CLOSE_CURLY}, la));
                    return false;
            }
        }
    }

    /**
     * Matches an attribute:
     *
     *     attribute ::= attributeKey '=' attributeValue
     *     attributeKey ::= ':' IDENTIFIER
     *     attributeValue ::= LITERAL | boolean | memberList
     *
     * @param parent The parent element to which the attribute belongs.
     * @return {@code true} if the attribute is matched successfully, {@code false} otherwise.
     */
    private boolean matchAttribute(IndexElement parent) {
        final AttributeKey key = AttributeKey.fromText(la.text);
        if (key == AttributeKey.UNKNOWN) {
            reportError(la.line, "Unknown attribute '" + la.text + "'.");
        } else if (parent.isAssociatedKey(key)) {
            reportError(la.line, "Duplicate attribute: '" + key.text + "'.");
        }
        consume();

        if (predict(Token.Type.EQ)) {
            consume();
        }

        if (!AttributeKey.isTokenTypeAllowedForKey(key, la.type)) {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append("Illegal value for attribute key: '").append(key.text).append("'.");
            if (AttributeKey.expectedTokenTypeFor(key) != null) {
                errorMessage.append("Expected: '").append(AttributeKey.expectedTokenTypeFor(key)).append("'.");
            }
            reportError(la.line,  errorMessage.toString());
        }

        switch (la.type) {

            case LITERAL:
                parseResult.addReference(parent, Pair.of(key, la.text));
                consume();
                return true;

            case OPEN_PAREN:
                int openParenLineNr = la.line;
                enterBlockOrList();
                return matchMembersList(parent, openParenLineNr);

            case BOOLEAN_VALUE:
                    parent.assoc(key, Boolean.parseBoolean(la.text));
                    consume();
                    return true;

            default:
                reportError(la.line, buildUnexpectedTokenMultipleErrorMessage(new Token.Type[]{Token.Type.LITERAL, Token.Type.OPEN_PAREN, Token.Type.BOOLEAN_VALUE}, la));
                return false;
        }
    }

    /**
     * Matches a members list:
     *
     *     memberList ::= '[' member (',' member)* ']'
     *
     * @param parent The parent of the members.
     * @param openParenLineNr The line number of the start of the list.
     * @return {@code true} if the match was successful, {@code false} otherwise.
     */
    private boolean matchMembersList(IndexElement parent, int openParenLineNr) {

        if (la.type == Token.Type.CLOSE_PAREN) {
            reportError(la.line, "Empty member lists are not allowed.");
            leaveBlockOrList();
            return false;
        }

        for (;;) {
            switch (la.type) {

                case LITERAL:
                case DIRECTIVE:
                    IndexElement member = matchMember();
                    acceptIfNonNull(parent::addMember, member);
                    if (la.type == Token.Type.CLOSE_PAREN) {
                        leaveBlockOrList();
                        return true;
                    } else if (predict(Token.Type.COMMA)) {
                        consume();
                    }

                    break;

                case EOF:
                    reportEOF("Unclosed member list. The opening parenthesis was on line: " + openParenLineNr + ".");
                    return false;

                default:
                    String errorMessage =
                            buildUnexpectedTokenMultipleErrorMessage(
                                    new Token.Type[]{Token.Type.DIRECTIVE, Token.Type.LITERAL, Token.Type.COMMA, Token.Type.CLOSE_PAREN},
                                    la
                            );
                    reportError(la.line, errorMessage);
            }
        }
    }

    /**
     * Matches a member element:
     *
     *     member ::= directive? element
     *
     * @return The matched {@link IndexElement}.
     */
    private IndexElement matchMember() {

        final Directive directive;
        if (la.type == Token.Type.DIRECTIVE) {
            directive = matchDirective();
            if (!directive.isPositionDirective() && directive != Directive.UNKNOWN) {
                // We don't add an error for illegal use in this context if the directive is UNKNOWN since directives of
                // that type are illegal in any case, and an error has already been added for it by matchDirective().
                reportError(
                        la.line,
                        buildIllegalUseOfDirectiveErrorMessage(directive, "only directives marking the relative position of the member to the parent are allowed here.")
                );
            }
            consume(); // Consume the 'DIRECTIVE' token.
        } else {
            directive = Directive.NONE;
        }

        if (!predict(Token.Type.LITERAL)) {
            consume();
            return null;
        }

        IndexElement member = matchElement();

        if (member != null) {
            member.assocPositionDirective(directive);
            if (member.hasSynonyms()) {
                for (IndexElement memberSynonym : member.getSynonyms()) {
                    memberSynonym.assocPositionDirective(directive);
                }
            }
        }

        return member;
    }

    /**
     * Matches a synonyms list:
     *
     *     synonymsList ::= '(' synonym (',' synonym)* ')'
     *
     * @param parent The parent element of the synonyms.
     * @param openBraceLineNr The line number of the start of the synonyms list.
     * @return {@code true} if matched successfully, {@code false} otherwise.
     */
    private boolean matchSynonymsList(IndexElement parent, int openBraceLineNr) {

        if (la.type == Token.Type.CLOSE_PAREN) {
            reportError(openBraceLineNr, "Empty synonyms lists are not allowed.");
            leaveBlockOrList();
            return false;
        }

        for (;;) {
            switch (la.type) {

                case DIRECTIVE:
                case LITERAL:
                    IndexElement synonym = matchSynonym();
                    acceptIfNonNull(parent::addSynonym, synonym);

                    if (la.type == Token.Type.CLOSE_PAREN) {
                        leaveBlockOrList();
                        return true;
                    } else if (predict(Token.Type.COMMA)) {
                        consume();
                    }
                    break;

                case EOF:
                    reportEOF("Unclosed synonyms list. The opening parenthesis was on line: " + openBraceLineNr + ".");
                    return false;

                default:
                    String errorMessage =
                            buildUnexpectedTokenMultipleErrorMessage(
                                    new Token.Type[]{Token.Type.COMMA, Token.Type.CLOSE_PAREN, Token.Type.DIRECTIVE, Token.Type.LITERAL},
                                    la
                            );
                    reportError(la.line, errorMessage);
                    consumeUntilNextTopLevelLiteral();
            }
        }
    }

    /**
     * Matches a synonym element:
     *
     *     synonym ::= directive? LITERAL
     *
     * @return The matched {@link IndexElement}.
     */
    private IndexElement matchSynonym() {

        final Directive directive;
        if (la.type == Token.Type.DIRECTIVE) {
            directive = matchDirective();
            if (!directive.isSetOperatorDirective() && directive != Directive.UNKNOWN) {
                // We don't want to add an 'illegal use of directive' message if the directive is UNKNOWN, because
                // matchDirective already added a message for the unknown directive itself. Stating that the illegal
                // directive is also illegal in this particular spot is redundant and possible confusing.
                reportError(la.line, buildIllegalUseOfDirectiveErrorMessage(directive, "only set operator directives are allowed here."));
            }
            consume();
        } else {
            directive = Directive.SET_UNION;
        }

        if (!predict(Token.Type.LITERAL)) {
            consume();
            return null;
        }

        IndexElement synonym = IndexElement.of(la);
        synonym.assocSetOperatorDirective(directive);
        consume();

        return synonym;
    }

    /**
     * Matches a directive:
     *
     *     directive ::= '@' IDENTIFIER
     *
     * @return The matched {@link Directive}.
     */
    private Directive matchDirective() {
        Directive directive = Directive.fromString(la.text);
        if (directive == Directive.UNKNOWN) {
            reportError(la.line, "Invalid specification directive: '" + la.text + "'.");
        }
        return directive;
    }

    private String buildUnexpectedTokenErrorMessage(Token.Type expectedType, Token providedToken) {
        StringBuilder message = new StringBuilder("Unexpected token. Expected a token of type: '")
                .append(expectedType.name())
                .append("', but received a token of type: '")
                .append(providedToken.type)
                .append("'. (Token content: '")
                .append(providedToken.text)
                .append("'.)");

        return message.toString();
    }

    private String buildUnexpectedTokenMultipleErrorMessage(Token.Type[] possibleTypes, Token providedToken) {
        StringBuilder message = new StringBuilder("Unexpected token. Expected a token of one of the following types:\n");

        for (Token.Type expected : possibleTypes) {
            message.append("'").append(expected.name()).append("', ");
        }

        message.append("\nbut received a token of type: '")
                .append(providedToken.type.name())
                .append("'. (Token content: '")
                .append(providedToken.text)
                .append("'.)");

        return message.toString();
    }

    private String buildIllegalUseOfDirectiveErrorMessage(Directive offendingDirective, String hintOfProperUse) {
        return "Illegal use of directive '" + offendingDirective.text + "', " + hintOfProperUse;
    }

    private void reportError(int line, String message) {
        RecognitionError error = new RecognitionError(line, message);
        parseResult.addRecognitionError(error);
    }

    private void reportEOF(String additionalInfo) {
        StringBuilder errorMessage = new StringBuilder("Unexpected end of input.");
        if (Checks.isNotNullOrBlankString(additionalInfo)) {
            errorMessage.append("\n\t").append(additionalInfo);
        }
        reportError(la.line, errorMessage.toString());
    }

    private boolean predict(Token.Type predicted) {
        if (predicted == la.type) {
            return true;
        } else {
            reportError(la.line, buildUnexpectedTokenErrorMessage(predicted, la));
            return false;
        }
    }

    private void consume() {
        Token t = lexer.nextToken();
        while (t.type == Token.Type.LEXER_ERROR) {
            t = lexer.nextToken();
        }
        if (parseResult.getRecognitionErrors().size() > 20) {
            throw new CancelParseException();
        }
        la = t;
    }

    private void consumeUntilNextTopLevelLiteral() {
        while (la.type != Token.Type.EOF) {
            if (listOrBlockDelimiterDepth == 0 && la.type == Token.Type.LITERAL) {
                return;
            } else {
                consume();
            }
        }
    }

    private void enterBlockOrList() {
        listOrBlockDelimiterDepth++;
        consume();
    }

    private void leaveBlockOrList() {
        listOrBlockDelimiterDepth--;
        assert listOrBlockDelimiterDepth >= 0;
        consume();
    }

    private <T> void acceptIfNonNull(Consumer<T> consumer, T t) {
        if (t != null) {
            consumer.accept(t);
        }
    }
}
