/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.validation;

import ltoorn.genix.dsl.shared.ValidationMessage;
import ltoorn.genix.dsl.shared.ValidationResult;
import ltoorn.genix.dsl.specification.ir.AttributeKey;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.dsl.specification.parsing.ParseResult;
import ltoorn.genix.dsl.shared.RecognitionError;
import ltoorn.genix.dsl.specification.parsing.SpecificationParser;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Pair;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SpecificationValidator {

    private SpecificationValidator() {}

    public static ValidationResult<List<IndexElement>> validate(String input) {
        SpecificationParser parser = new SpecificationParser(input);
        final ParseResult parseResult = parser.parse();
        final ValidationResult<List<IndexElement>> result = ValidationResult.fromParseResult(parseResult);

        for (RecognitionError recognitionError : parseResult.getRecognitionErrors()) {
            result.addError(ValidationMessage.fromRecognitionError(recognitionError));
        }

        if (result.isSuccessful()) {
            resolveReferences(result, parseResult);
            resolveNoDuplicates(result, parseResult.getResult());
            resolveWarnings(result, parseResult.getResult());
        } else {
            result.addError(
                    new ValidationMessage(
                            "Validation of specification file has been aborted due to previously encountered errors."
                            , parseResult.getLineNrOfLastAddedError()
                    )
            );
        }

        return result;
    }

    private static void resolveReferences(ValidationResult<List<IndexElement>> validationResult, ParseResult parseResult) {

        for (Pair<IndexElement, Pair<AttributeKey, String>> refPair : parseResult.getReferences()) {
            final IndexElement baseElement = refPair.getLeft();
            final AttributeKey refKey = refPair.getRight().getLeft();
            final String refContent = refPair.getRight().getRight();

            IndexElement reference = null;

            for (IndexElement element : parseResult.getResult()) {
                if (element.contentForOutput.equals(refContent)) {
                    reference = element;
                    break;
                }
            }

            if (reference == null) {
                validationResult.addError(
                        new ValidationMessage(
                                "Invalid reference: '"
                                        + baseElement.contentForOutput
                                        + "' -> '"
                                        + refContent
                                        + "'. The referred element is not a top level (i.e. primary) element in this specification."
                                , baseElement.lineNumber
                        )
                );
                return;
            }

            if (baseElement.isAssociatedKey(refKey) && refKey == AttributeKey.HARD_REFERENCE) {
                validationResult.addError(
                        new ValidationMessage(
                                "Trying to add multiple hard references to element: '"
                                        + baseElement.contentForOutput
                                        + "'. Multiple hard references are not allowed."
                                , baseElement.lineNumber
                        )
                );
                return;
            }

            if (refKey == AttributeKey.SOFT_REFERENCE && baseElement.hasSoftReference()) {
                if (baseElement.getSoftReferences().stream().anyMatch(r -> r.contentForOutput.equals(refContent))) {
                    validationResult.addError(
                            new ValidationMessage(
                                    "Duplicate soft reference: '"
                                            + refContent
                                            + "', at element: '"
                                            + baseElement.contentForOutput
                                            + "'."
                                    , baseElement.lineNumber
                            )
                    );
                    return;
                }
            }

            baseElement.assoc(refKey, reference);
        }
    }

    private static void resolveNoDuplicates(ValidationResult<List<IndexElement>> validationResult, List<IndexElement> elements) {

        final Set<String> searchStrings = new HashSet<>();

        for (IndexElement element : elements) {
            if (searchStrings.contains(element.contentForSearch)) {
                validationResult.addError(
                        new ValidationMessage(
                                "Duplicate element: '"
                                        + element.contentForOutput
                                        + "'."
                                , element.lineNumber
                        )
                );
            } else {
                searchStrings.add(element.contentForSearch);
            }

            if (Checks.isNotNullOrEmptyCollection(element.getMembers())) {
                resolveNoDuplicates(validationResult, element.getMembers());
            }

            if (Checks.isNotNullOrEmptyCollection(element.getSynonyms())) {
                resolveNoDuplicates(validationResult, element.getSynonyms());
            }
        }
    }

    private static void resolveWarnings(ValidationResult<List<IndexElement>> validationResult, List<IndexElement> elements) {
        for (IndexElement element : elements) {
            if (element.isIndexable() && element.hasHardReference()) {
                validationResult.addWarning(
                        new ValidationMessage(
                                "The element '"
                                        + element.contentForOutput
                                        + "' has a hard reference while it's also indexable."
                                , element.lineNumber
                        )
                );
            }
            if (element.hasMembers()) {
                resolveWarnings(validationResult, element.getMembers());
            }
        }
    }
}
