/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.searching;

import ltoorn.genix.context.Context;
import ltoorn.genix.dsl.specification.ir.Directive;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Contracts;
import ltoorn.genix.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;

/**
 * Provides functions for searching for occurrences of {@link IndexElement} in the pages of the
 * text to be indexed.
 */
public class SearchIndexElements {

    private SearchIndexElements() {}

    /**
     * Finds occurrences for each {@link IndexElement} in {@code elements} on the {@code pages}.
     * This method adds all occurrences to the {@link IndexElement} to which they belong, in
     * order (thus mutating the {@link IndexElement} as necessary).
     * @param elements The list of {@link IndexElement} for which we need to find occurrences in
     *                 the {@code pages}; the {@code elements} are assumed to be non-empty.
     * @param pages The list of {@code pages} in which we need to look for occurrences of the
     *              {@code elements}; the {@code pages} are assumed to be non-empty.
     * @return The, if necessary mutated, {@code elements}; ensuring the {@code elements} to be non-empty.
     */
    public static List<IndexElement> findOccurrences(List<IndexElement> elements, List<String> pages) {
        Contracts.assumeAll(Checks.isNotNullOrEmptyCollection(elements), Checks.isNotNullOrEmptyCollection(pages));
        final List<int[]> pagesCodePoints = SearchUtil.stripHyphensFromPages(pages);
        for (int i = 0; i < pagesCodePoints.size(); i++) {
            // We only search for elements on indexable pages.
            if (isPageWithinIndexableRange(i, Context.get().config.getIndexableRange())) {
                for (IndexElement element : elements) {
                    findOccurrencesForElement(element, i, pagesCodePoints.get(i));
                }
            }
        }
        return Contracts.require(elements, Checks::isNotNullOrEmptyCollection);
    }

    /**
     * Tries to find the content of an element on a given page. It performs a rather simple
     * brute force search through the code points of the page content.
     * <p>
     * The algorithm works as follows:
     * <ul>
     *     <li>Iterate through the {@code pageCodePoints}: at each iteration of the outer loop,
     *     initialize the index into the {@code elementCodePoints} to 0.</li>
     *     <li>Move through the code points of the {@code elementCodePoints} in the inner loop
     *     checking them for equality against the code points of the {@code pageCodePoints} one
     *     by one:
     *     <ul>
     *         <li>if we reach the end of the {@code elementCodePoints} array <em>and</em> we are
     *         at a word delimiting character in (or have reached the end of) the {@code pageCodePoints}
     *         then we have found a match: return {@code true} from the method.</li>
     *         <li>if both the element code point and the page code point are a white space character,
     *         then we consume all whitespace until we hit a non whitespace character respectively. We
     *         do this to enable the matching algorithm to handle different variations of whitespace
     *         within a search term. The most common such case would be a new line within a multi-word
     *         search term: if we're looking for 'big houses', then 'big \n houses', should also match.</li>
     *         <li>else we have encountered a pair of characters that didn't test equal (and
     *         thus the word we're looking for can't appear at the current location of the page),
     *         at which point we exit the inner loop.</li>
     *     </ul></li>
     *     <li>Finally, as the last action in the outer loop, we advance the {@code pgIndex}.
     *     Because we know that we can never match the {@code elementContent} in the middle of a
     *     word, we advance to the end of the current word in {@code pageCodePoints}, or
     *     we advance by 1 in case we're already at a word delimiting character. (See:
     *     {@link SearchUtil#advanceIndex(int, int[])}</li>
     *     <li>Whenever we drop out of the outer loop, we know that we failed to match the
     *     {@code elementContent} in the {@code pageContent}, so we return {@code false}.</li>
     * </ul>
     * <p>
     * @param elementContent The text we're looking for.
     * @param pageCodePoints The code points of the page on which we're looking for the {@code elementContent}.
     * @return {@code true} if the elementContent occurs on the page, {@code false} otherwise.
     */
    static boolean findElementContentOnPage(String elementContent, int[] pageCodePoints) {
        final int[] elementCodePoints = elementContent.codePoints().toArray();

        int pgIndex = 0;
        while (pgIndex < pageCodePoints.length) {

            int elIndex = 0;
            while (pgIndex < pageCodePoints.length && elIndex < elementCodePoints.length) {

                if (Character.isWhitespace(elementCodePoints[elIndex]) && Character.isWhitespace(pageCodePoints[pgIndex])) {
                    while (elIndex < elementCodePoints.length && Character.isWhitespace(elementCodePoints[elIndex])) {
                        elIndex++;
                    }
                    while (pgIndex < pageCodePoints.length && Character.isWhitespace(pageCodePoints[pgIndex])) {
                        pgIndex++;
                    }
                }

                if (pgIndex < pageCodePoints.length && elIndex < elementCodePoints.length) {
                    if (!SearchUtil.codePointsEqualIgnoreCase(pageCodePoints[pgIndex], elementCodePoints[elIndex])) {
                        break;
                    }
                    pgIndex++;
                    elIndex++;
                    if (elIndex == elementCodePoints.length) {
                        if (pgIndex == pageCodePoints.length || SearchUtil.isWordDelimitingChar(pageCodePoints[pgIndex])) {
                            return true;
                        }
                    }
                }
            }

            pgIndex = SearchUtil.advanceIndex(pgIndex, pageCodePoints);
        }

        return false;
    }

    private static void findOccurrencesForElement(IndexElement element, int pageNumber, int[] pageContent) {
        if (element.isIndexable()) {

            addOccurrence(pageNumber, pageContent, element);

            if (element.hasSynonyms()) {
                for (IndexElement synonym : element.getSynonyms()) {
                    addOccurrence(pageNumber, pageContent, synonym);
                }
            }
        }

        if (element.hasMembers()) {
            for (IndexElement member : element.getMembers()) {
                addOccurrenceForMember(pageNumber, pageContent, element, member);
            }
        }
    }

    private static void addOccurrence(int pageNumber, int[] pageContent, IndexElement element) {
        addOccurrenceHelper(pageNumber, pageContent, element, buildPredicate(element.contentForSearch));
    }

    private static void addOccurrenceForMember(int pageNumber, int[] pageContent, IndexElement parent, IndexElement member) {
        if (member.getPositionDirective() == Directive.NONE) {
            findOccurrencesForElement(member, pageNumber, pageContent);
        } else {
            addOccurrenceHelper(pageNumber, pageContent, member, determinePredicateForMember(parent, member));
        }
    }

    private static void addOccurrenceHelper(
            int pageNumber,
            int[] pageContent,
            IndexElement element,
            Predicate<int[]> predicate
    ) {
        if (predicate.test(pageContent)) {
            element.addPage(pageNumber);
        }
    }

    private static Predicate<int[]> buildPredicate(String searchString) {
        return page -> findElementContentOnPage(searchString, page);
    }

    private static Predicate<int[]> buildPredicate(List<String> searchStrings) {
        return page -> searchStrings.stream().anyMatch(searchString -> findElementContentOnPage(searchString, page));
    }

    private static Predicate<int[]> determinePredicateForMember(IndexElement parent, IndexElement member) {
        List<String> searchStrings = new ArrayList<>();

        buildListOfSearchStringsForMember(searchStrings, parent, member);

        return buildPredicate(searchStrings);
    }

    private static void buildListOfSearchStringsForMember(List<String> searchStrings, IndexElement parent, IndexElement member) {

        BiFunction<String, String, String> appendWithSpace = (l, r) -> l + " " + r;

        final String parentContent = parent.contentForSearch;
        final String memberContent = member.contentForSearch;

        switch (member.getPositionDirective()) {

            case LEFT:
                searchStrings.add(appendWithSpace.apply(memberContent, parentContent));
                break;

            case RIGHT:
                searchStrings.add(appendWithSpace.apply(parentContent, memberContent));
                break;

            case LEFT_OR_RIGHT:
                searchStrings.add(appendWithSpace.apply(memberContent, parentContent));
                searchStrings.add(appendWithSpace.apply(parentContent, memberContent));
                break;

            default: throw new IllegalStateException();
        }

        if (parent.hasSynonyms()) {
            for (IndexElement parentSynonym : parent.getSynonyms()) {
                buildListOfSearchStringsForMember(searchStrings, parentSynonym, member);
            }
        }

        if (member.hasSynonyms()) {
            for (IndexElement memberSynonym : member.getSynonyms()) {
                buildListOfSearchStringsForMember(searchStrings, parent, memberSynonym);
            }
        }
    }

    private static boolean isPageWithinIndexableRange(int i, Pair<Integer, Integer> range) {
        return Checks.isNull(range) ||
                ((Checks.isNull(range.getLeft()) || i >= range.getLeft()-1) &&
                        (Checks.isNull(range.getRight()) || i <= range.getRight()-1));
    }
}
