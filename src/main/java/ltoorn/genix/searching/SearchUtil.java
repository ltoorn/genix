/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.searching;

import java.util.ArrayList;
import java.util.List;

public class SearchUtil {

    private SearchUtil() {}

    /**
     * Strips all hyphens from the provided pages.
     * @param pages The pages to process.
     * @return A list of code points arrays without hyphens.
     */
    public static List<int[]> stripHyphensFromPages(List<String> pages) {
        List<int[]> processedCodePoints = new ArrayList<>();
        for (String p : pages) {
            int[] pageCodePoints = stripHyphens(p).codePoints().toArray();
            processedCodePoints.add(pageCodePoints);
        }
        return processedCodePoints;
    }

    /**
     * Strips all hyphens from the provided String. All single hyphens and hyphens followed directly by a newline
     * ('\n') are simply dropped, all double hyphens are replaced by a single space (' '). Double hyphens are matched
     * from left to right.
     * @param s The String to strip.
     * @return A String with all code points from {@code s} without hyphens.
     */
    public static String stripHyphens(String s) {
        final int[] codePoints = s.codePoints().toArray();
        final StringBuilder builder = new StringBuilder();
        int la = 0;
        while (la < codePoints.length) {
            if (isHyphen(codePoints[la])) {
                la++;
                if (la < codePoints.length) {
                    if (isHyphen(codePoints[la])) {
                        la++;
                        builder.append(" ");
                    } else if (codePoints[la] == '\n') {
                        la++;
                    }
                }
            } else {
                builder.appendCodePoint(codePoints[la]);
                la++;
            }
        }
        return builder.toString();
    }

    /**
     * Advances the index. If we are in the middle of a word, then we advance to the end of the
     * word; otherwise we advance a single character.
     * @param index The index into the the array of code points.
     * @param codePoints The array of code points of the text in which we need to search for occurrences.
     * @return The advanced index.
     */
    public static int advanceIndex(int index, int[] codePoints) {
        int endOfWord = consumeWord(index, codePoints);
        if (endOfWord == index) { endOfWord++; }
        while (endOfWord < codePoints.length && Character.isWhitespace(codePoints[endOfWord])) { endOfWord++; }
        return endOfWord;
    }

    /**
     * Consumes a word. We iterate through the {@code codePoints} until we encounter
     * a 'word delimiting character'. (See: {@link #isWordDelimitingChar(int)}).
     * @param index The index into the {@code codePoints}.
     * @param codePoints The code points we're searching through.
     * @return The index of the first 'word delimiting character' we find.
     */
    public static int consumeWord(int index, int[] codePoints) {
        while (index < codePoints.length && !isWordDelimitingChar(codePoints[index])) {
            ++index;
        }
        return index;
    }

    /**
     * Determines whether the provided code point represents a word delimiting character.
     * <p>
     * A character is word delimiting when it's <em>not</em> a hyphen <em>and</em> either
     * <ul>
     *     <li>a space character;</li>
     *     <li>a control character;</li>
     *     <li>a punctuation character (that is, belonging to one of the standard Unicode punctuation categories);</li>
     *     <li>a symbol (that is, belonging to one of the standard Unicode symbol categories); or</li>
     *     <li>a separator character (that is, belonging to one of the standard Unicode separator categories).</li>
     * </ul>
     * <p>
     * Since we use this method in {@link #consumeWord(int, int[])}, it's very important that we don't consider
     * any character part of a word when it isn't, since that could cause false negatives. We therefore consider a
     * large range of characters to be 'word delimiting'. Word delimiting characters can still be used as part of
     * an element's content (because the matching process, doesn't stop whenever a word delimiting character is
     * encountered).
     * <p>
     * @param c The code point under test.
     * @return {@code true} if the code point is a word delimiter, {@code false} otherwise.
     */
    public static boolean isWordDelimitingChar(int c) {
        return !isHyphen(c) &&
                (Character.isSpaceChar(c) ||
                        Character.isISOControl(c) ||
                        isPunctuation(c) ||
                        isSymbol(c) ||
                        isSeparator(c));
    }

    public static boolean isHyphen(int c) {
        return c == 0x2D ||  // hyphen-minus (ASCII hyphen '-')
                c == 0xAD || // soft hyphen
                c == 0x2010 || // hyphen
                c == 0x2011;   // non-breaking hyphen
    }

    public static boolean isPunctuation(int c) {
        switch (Character.getType(c)) {
            case Character.CONNECTOR_PUNCTUATION:
            case Character.DASH_PUNCTUATION:
            case Character.START_PUNCTUATION:
            case Character.END_PUNCTUATION:
            case Character.INITIAL_QUOTE_PUNCTUATION:
            case Character.FINAL_QUOTE_PUNCTUATION:
            case Character.OTHER_PUNCTUATION:
                return true;
            default:
                return false;
        }
    }

    public static boolean isSymbol(int c) {
        switch (Character.getType(c)) {
            case Character.MATH_SYMBOL:
            case Character.CURRENCY_SYMBOL:
            case Character.MODIFIER_SYMBOL:
            case Character.OTHER_SYMBOL:
                return true;
            default:
                return false;
        }
    }

    public static boolean isSeparator(int c) {
        switch (Character.getType(c)) {
            case Character.SPACE_SEPARATOR:
            case Character.LINE_SEPARATOR:
            case Character.PARAGRAPH_SEPARATOR:
                return true;
            default:
                return false;
        }
    }

    public static boolean codePointsEqualIgnoreCase(int a, int b) {
        return Character.toLowerCase(a) == Character.toLowerCase(b);
    }
}
