/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.context;

import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.util.Checks;
import ltoorn.genix.util.Contracts;

/**
 * Holds contextual data which needs to be available during execution of the program.
 * This class can only be instantiated once and must remain valid for entire execution
 * of the rest of the program.
 */
public class Context {

    /**
     * The {@link UserConfig} which should be known at the time of initialization of this
     * {@code Context}.
     */
    public final UserConfig config;

    private static Context instance;

    private Context(UserConfig config) {
        this.config = config;
    }

    /**
     * Initializes the {@code Context} with the provided {@code config}.
     * May only be called once, on an uninitialized {@code Context}.
     * @param config The {@link UserConfig}, must not be {@code null}
     */
    public static void initialize(UserConfig config) {
        Contracts.assume(Checks.isNull(instance));
        instance = new Context(Contracts.require(config, Checks::isNotNull));
    }

    /**
     * Gets the instance of the {@code Context}. May not be called on an uninitialized {@code Context}
     * @return The instance of the {@code Context}
     */
    public static Context get() {
        return Contracts.require(instance, Checks::isNotNull, "Tried to get uninitialized Context.");
    }
}
