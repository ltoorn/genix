/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.genix.context.configuration;

import ltoorn.genix.util.Pair;

import java.util.HashMap;

/**
 * Class to hold the user configuration as it will be used throughout the execution of the program in memory.
 *
 * All values are assumed to be validated by the {@link ltoorn.genix.dsl.configuration.parsing.ConfigurationParser} and
 * the {@link ltoorn.genix.dsl.configuration.validation.ConfigurationValidator} before they are added to this class.
 */
public class UserConfig {

	private final HashMap<ConfigOption, Object> configMap;

	public UserConfig() {
		configMap = new HashMap<>();
	}

	public static UserConfig defaultUserConfig()  {
		return new UserConfig();
	}

	public String getIndexHeaderSpacing() {
	    return (String) get(ConfigOption.INDEX_HEADER_SPACING);
    }

	public String getPageNumbersStartDelimiter() {
		return (String) get(ConfigOption.PAGE_NUMBERS_START_DELIMITER);
	}

	public String getPageNumberSeparator() {
		return (String) get(ConfigOption.PAGE_NUMBER_SEPARATOR);
	}

	public String getHardReferenceDelimiter() {
		return (String) get(ConfigOption.HARD_REFERENCE_DELIMITER);
	}

	public String getSoftReferenceDelimiter() {
		return (String) get(ConfigOption.SOFT_REFERENCE_DELIMITER);
	}

	public Integer getNumberOfLeadingDummyPages() {
	    return (Integer) get(ConfigOption.NUMBER_OF_LEADING_DUMMY_PAGES);
    }

    public Integer getNumberOfTopLinesToSkipPerPage() {
		return (Integer) get(ConfigOption.NUMBER_OF_TOP_LINES_TO_SKIP_PER_PAGE);
    }

    public Integer getNumberOfBottomLinesToSkipPerPage() {
	    return (Integer) get(ConfigOption.NUMBER_OF_BOTTOM_LINES_TO_SKIP_PER_PAGE);
    }

    @SuppressWarnings("unchecked")
    public Pair<Integer, Integer> getIndexableRange() {
        return (Pair<Integer, Integer>) get(ConfigOption.INDEXABLE_RANGE);
    }

	public Integer getRomanNumeralEnd() {
	    return (Integer) get(ConfigOption.ROMAN_NUMERAL_END);
    }

    public boolean getRomanNumeralUpperCase() {
	    return Boolean.TRUE.equals(get(ConfigOption.IS_ROMAN_NUMERAL_UPPER_CASE));
    }

    public String getSequenceConnector() {
	    return (String) get(ConfigOption.SEQUENCE_CONNECTOR);
    }

    public void addOption(ConfigOption option, Object value) {
	    configMap.put(option, value);
    }

	private Object get(ConfigOption key) {
		if (!configMap.containsKey(key)) {
			return key.defaultValue;
		} else {
			return configMap.get(key);
		}
	}
}
