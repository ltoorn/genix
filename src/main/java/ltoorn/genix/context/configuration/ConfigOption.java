/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.context.configuration;

/**
 * Pre-defined configuration options, with their default values. The user may omit any or all config options, in
 * which case the default values defined here will be used.
 */
public enum ConfigOption {

    /**
     * Marks the separation between an element and its page numbers or reference in the output.
     */
	PAGE_NUMBERS_START_DELIMITER("pageNumbersStartDelimiter", ","),

    /**
     * Separates the page numbers in a page numbers list of an element in the output.
     */
	PAGE_NUMBER_SEPARATOR("pageNumberSeparator", ","),

    /**
     * Refers to the reference of an element in the output.
     */
    HARD_REFERENCE_DELIMITER("hardReferenceDelimiter", "see:"),

    /**
     * Refers to the secondary reference(s) of an element int the output.
     */
    SOFT_REFERENCE_DELIMITER("softReferenceDelimiter", "see also:"),

    /**
     * Separates the different lexicographical groupings in the output.
     */
    INDEX_HEADER_SPACING("indexHeaderSpacing", "\n"),

    /**
     * Denotes the number of _physical_ pages that occur in the PDF before the actual first page of the document according to its page numbers.
     */
    NUMBER_OF_LEADING_DUMMY_PAGES("numberOfLeadingDummyPages", 0),

    /**
     * Denotes the number lines per page, counted from the top of the page, that will be excluded from the search.
     */
    NUMBER_OF_TOP_LINES_TO_SKIP_PER_PAGE("numberOfTopLinesToSkipPerPage", 0),

    /**
     * Denotes the number of lines per page, counted from the bottom of the page, that will be excluded from the search.
     */
    NUMBER_OF_BOTTOM_LINES_TO_SKIP_PER_PAGE("numberOfBottomLinesToSkipPerPage", 0),

    /**
     * Denotes the start (inclusive) and the end (exclusive) of the range of pages that are to be indexed by the program.
     * All pages that lie outside of this range will be excluded from search.
     */
    INDEXABLE_RANGE("indexableRange", null),

    /**
     * Denotes the page number of the last page that carries a roman numeral in the document, should such pages exist.
     * All matches on pages from the first page up to and including the {@code romanNumeralEnd} will appear as roman
     * numerals in the output. The program assumes that the page count after the {@code romanNumeralEnd} page recommence
     * with the hindu-arabic numeral '1'.
     */
    ROMAN_NUMERAL_END("romanNumeralEnd", null),

    /**
     * Denotes whether the roman numerals appearing in the output should be in upper case or not. The option will have no
     * effect when {@code romanNumeralEnd} is {@code null}.
     */
    IS_ROMAN_NUMERAL_UPPER_CASE("isRomanNumeralUpperCase", false),

    /**
     * Connects the beginning and end of a contiguous sequence of page numbers in the output.
     */
    SEQUENCE_CONNECTOR("sequenceConnector", "-");

    public enum ValueType {
        INTEGER, STRING, BOOLEAN, PAIR
    }

    final String name;
	final Object defaultValue;

	ConfigOption(String name, Object defaultValue) {
	    this.name = name;
		this.defaultValue = defaultValue;
	}

	public static ConfigOption fromName(String name) {
	    for (ConfigOption option : ConfigOption.values()) {
            if (option.name.equalsIgnoreCase(name)) {
                return option;
            }
        }
	    return null;
    }

    public static ValueType getValueTypeForOption(ConfigOption option) {
	    switch (option) {
            case PAGE_NUMBERS_START_DELIMITER:
            case PAGE_NUMBER_SEPARATOR:
            case HARD_REFERENCE_DELIMITER:
            case SOFT_REFERENCE_DELIMITER:
            case INDEX_HEADER_SPACING:
            case SEQUENCE_CONNECTOR:
                return ValueType.STRING;

            case ROMAN_NUMERAL_END:
            case NUMBER_OF_LEADING_DUMMY_PAGES:
            case NUMBER_OF_TOP_LINES_TO_SKIP_PER_PAGE:
            case NUMBER_OF_BOTTOM_LINES_TO_SKIP_PER_PAGE:
                return ValueType.INTEGER;

            case INDEXABLE_RANGE:
                return ValueType.PAIR;

            case IS_ROMAN_NUMERAL_UPPER_CASE:
                return ValueType.BOOLEAN;

            default:
                throw new IllegalArgumentException();
        }
    }
}
