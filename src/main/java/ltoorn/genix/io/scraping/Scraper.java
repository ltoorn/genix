/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.io.scraping;

import ltoorn.genix.context.Context;
import ltoorn.genix.io.Output;
import ltoorn.genix.searching.SearchUtil;
import ltoorn.genix.util.Checks;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Scraper {

    private Scraper() {}

    public static List<String> getTextPerPdfPage(File file) {

        try (final PDDocument document = PDDocument.load(file)) {

            final ArrayList<String> pages = new ArrayList<>();
            final PDFTextStripper textStripper = new PDFTextStripper();
            final Splitter splitter = new Splitter();
            final List<PDDocument> pdfPages = splitter.split(document);

            for (int i = 0; i < pdfPages.size(); i++) {
                if (i >= Context.get().config.getNumberOfLeadingDummyPages()) {
                    try (final PDDocument page = pdfPages.get(i)) {
                        pages.add(textStripper.getText(page));
                    }
                }
            }

            return pages;
        } catch (IOException e) { // todo: write exception to log
            Output.ioErrorMessage("failed to extract text from PDF");
            return null;
        }
    }

    public static String getFullPdfText(File file) {

        try (final PDDocument document = PDDocument.load(file)) {

            final PDFTextStripper textStripper = new PDFTextStripper();
            final String text = textStripper.getText(document);
            document.close();

            return text;
        } catch (IOException e) { // todo: write exception to log
            Output.ioErrorMessage("failed to extract text from PDF");
            return null;
        }
    }

    /**
     * Extracts a list of unique words from the input text and sorts them alphabetically. Any sequence of characters
     * the is delimited by a 'word delimiting character' is considered a word (see: {@link SearchUtil#isWordDelimitingChar(int)}.
     * The list of words can serve as raw material for the user to help with writing the index spec.
     * @param text The input text.
     * @return The list of words, lexicographically sorted.
     */
    public static List<String> extractListOfWords(String text) {
        int[] codePoints      = text.codePoints().toArray();
        Set<String> wordSet   = new HashSet<>();
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < codePoints.length; i++) {
            int cp = codePoints[i];
            if (SearchUtil.isWordDelimitingChar(cp)) {
                String word = builder.toString();

                if (!word.isEmpty()) {
                    wordSet.add(word.toLowerCase());
                }
                builder.delete(0, builder.length());

                SearchUtil.advanceIndex(i, codePoints);
            } else {
                builder.appendCodePoint(cp);
            }
        }

        List<String> wordList = new ArrayList<>();
        for (String word : wordSet) {
            int c = word.codePointAt(0);
            if (Character.isDigit(c) || SearchUtil.isHyphen(c) || SearchUtil.isHyphen(word.codePointAt(word.length()-1))) {
                // To reduce the amount of noise in the word list, we filter out all words that start with a digit, or that
                // start or end with a hyphen.
                continue;
            }
            wordList.add(word);
        }
        wordList.sort(Comparator.naturalOrder());

        return wordList;
    }
    public static List<String> preProcessRawPages(List<String> rawPages) {
        if (Context.get().config.getNumberOfTopLinesToSkipPerPage() > 0 || Context.get().config.getNumberOfBottomLinesToSkipPerPage() > 0) {
            List<String> processedPages = new ArrayList<>();
            List<String> discardedText = new ArrayList<>();
            for (int i = 0; i < rawPages.size(); i++) {
                String rawPage = rawPages.get(i);
                int startSubString = computeStartSubString(rawPage, Context.get().config.getNumberOfTopLinesToSkipPerPage());
                int endSubString = computeEndSubString(rawPage, Context.get().config.getNumberOfBottomLinesToSkipPerPage());

                processedPages.add(rawPage.substring(startSubString, endSubString));

                String discardedTopLines = rawPage.substring(0, startSubString);
                String discardedBottomLines = rawPage.substring(endSubString);

                StringBuilder discarded = new StringBuilder();
                discarded.append("[page ").append(i+1).append("] ");
                if (Checks.isNotNullOrBlankString(discardedTopLines)) {
                    discarded.append("'").append(discardedTopLines).append("'");
                }
                if (Checks.isNotNullOrBlankString(discardedBottomLines)) {
                    discarded.append(" | ").append("'").append(discardedBottomLines).append("'");
                }
                discarded.append("\n");

                discardedText.add(discarded.toString());
            }

            // todo: create proper logging functionality.
            Output.outputContent(
                    discardedText.stream().reduce("", String::concat)
                    , "genix"
                    , "log of discarded text"
            );

            return processedPages;
        } else {
            return rawPages;
        }
    }

    private static int computeStartSubString(String s, int numberOfLinesToSkip) {
        int subStringStart = 0;
        int newLines = 0;
        for (int i = 0; i < s.length() && newLines != numberOfLinesToSkip; i++) {
            if (s.codePointAt(i) == '\n') {
                newLines++;
                subStringStart = i;
            }
        }
        return subStringStart;
    }

    private static int computeEndSubString(String s, int numberOfLinesToSkip) {
        int subStringEnd = s.length();
        int newLines = 0;
        for (int i = s.length()-1; i >= 0 && newLines != numberOfLinesToSkip; i--) {
            if (s.codePointAt(i) == '\n') {
                newLines++;
                subStringEnd = i;
            }
        }
        return subStringEnd;
    }
}
