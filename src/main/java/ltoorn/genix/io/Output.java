/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class Output {

    public enum MessageType { INFO, WARNING, ERROR }

    private Output() {}

    public static void printMessage(Output.MessageType type, String message) {

        String ANSI_RESET = "\u001B[0m";
        String ANSI_RED   = "\u001B[31m";
        String ANSI_CYAN  = "\u001B[36m";
        String ANSI_GREEN = "\u001B[32m";

        String prefix;
        switch (type) {

            case INFO:
                prefix = ANSI_GREEN + "Genix -- INFO:" + ANSI_RESET + "\n\t";
                break;

            case WARNING:
                prefix = ANSI_CYAN + "Genix -- WARNING:" + ANSI_RESET + "\n\t";
                break;

            case ERROR:
                prefix = ANSI_RED + "Genix -- ERROR:" + ANSI_RESET + "\n\t";
                break;

            default:
                throw new IllegalStateException();
        }

        System.out.println(prefix + message);
    }

    public static void outputContent(String content, String filenamePrefix, String contentName) {
        String filename = filenamePrefix + "_" + contentName.replace(" ", "_");
        String extension = ".txt";
        String completeFileName = filename + extension;

        File file = new File(completeFileName);
        int n = 0; // Make sure the filename is unique.
        while (file.exists()) {
            String numberedFileName = filename + "_" + n + extension;
            file = new File(numberedFileName);
            completeFileName = numberedFileName;
            ++n;
        }

        try {
            if (!file.createNewFile()) {
                ioErrorMessage("creating a new file");
                return;
            }
        } catch (IOException e) {
            ioErrorMessage("creating a new file");
            return;
        }

        if (!file.setWritable(true)) {
            Output.printMessage(Output.MessageType.ERROR, "Unable to get write access to file.");
            return;
        }

        try (final FileOutputStream fileOutputStream = new FileOutputStream(file);
             final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
             final BufferedWriter writer = new BufferedWriter(outputStreamWriter)) {

            writer.write(content);
            writer.flush();

            Output.printMessage(Output.MessageType.INFO, "Wrote " + contentName + " to '" + completeFileName + "'.");
        } catch (IOException e) {
            ioErrorMessage("outputting the " + contentName);
        }
    }

    public static void ioErrorMessage(String errorContext) {
        Output.printMessage(Output.MessageType.ERROR,
                "An error occurred while "
                        + errorContext
                        + ". Please try again or file a bug report if the problem persists.");
    }
}
