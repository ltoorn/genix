/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.util;

import java.util.Objects;

/**
 * A heterogeneous Pair of elements of type (T * U).
 * @param <T> The type of the left element
 * @param <U> The type of the right element
 */
public class Pair<T, U> {

    private final T left;
    private final U right;

    private int hashCode;

    private Pair(T left, U right) {
        this.left = left;
        this.right = right;

        hashCode = 0;
    }

    /**
     * Constructs a new {@code Pair} of the provided elements.
     * @param left The left element of the {@code Pair}
     * @param right The right element of the {@code Pair}
     * @param <T> The type of the left element
     * @param <U> The type of thr right element
     * @return The new {@code Pair}
     */
    public static <T, U> Pair<T, U> of(T left, U right) {
        return new Pair<>(left, right);
    }

    public T getLeft() {
        return left;
    }

    public U getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pair)) {
            return false;
        }
        Pair<?, ?> other = (Pair<?, ?>) o;
        return Objects.equals(this.left, other.left) && Objects.equals(this.right, other.right);
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = 31 * left.hashCode() + right.hashCode();
        }
        return hashCode;
    }
}
