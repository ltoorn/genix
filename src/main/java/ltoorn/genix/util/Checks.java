/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.util;

import java.util.Collection;

/**
 * Convenience class that holds functions that perform common checks.
 */
public class Checks {

    private Checks() {}

    public static <T> boolean isNotNullOrEmptyCollection(Collection<T> list) {
        return isNotNull(list) && !list.isEmpty();
    }

    public static boolean isNotNullOrBlankString(String s) {
        if (isNull(s) || s.isEmpty()) {
            return false;
        } else {
            return s.chars().anyMatch(c -> !Character.isWhitespace(c));
        }
    }

    public static <T> boolean isNull(T t) {
        return t == null;
    }

    public static <T> boolean isNotNull(T t) {
        return t != null;
    }
}
