/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.util;

import java.util.function.Predicate;

/**
 * Defines simple contracts that throw a {@link ContractViolationException} when
 * the provided Contract is violated.
 */
public class Contracts {

    private static final String defaultMessageAssumption = "Assumption violated.";

    private Contracts() {}

    /**
     * Throws a {@link ContractViolationException} if the {@code predicate} evaluates to {@code false},
     * otherwise returns the first argument unchanged.
     * @param t Argument to be passed on if the Contract is upheld.
     * @param predicate Performs some test on {@code t} to check the validity of the Contract.
     * @param message The error message passed to the {@link ContractViolationException}.
     * @param <T> The type of the first argument and of the return value.
     * @return The first argument, unchanged.
     */
    public static <T> T require(T t, Predicate<T> predicate, String message) {
        if (!predicate.test(t)) throw new ContractViolationException(message);
        return t;
    }

    /**
     * Requirement with default error message in case of violation;
     * See {@link Contracts#require(Object, Predicate, String)}
     */
    public static <T> T require(T t, Predicate<T> predicate) {
        final String defaultMessageRequirement = "Requirement violated.";
        return require(t, predicate, defaultMessageRequirement);
    }

    /**
     * Throws a {@link ContractViolationException} if the assumption that {@code predicate} holds
     * is violated.
     * @param predicate The predicate which is assumed to hold.
     * @param message The error message passed to the {@link ContractViolationException}
     */
    public static void assume(boolean predicate, String message) {
        if (!predicate) throw new ContractViolationException(message);
    }

    /**
     * Assumption with a default error message in case of violation;
     * See {@link Contracts#assume(boolean, String)}
     */
    public static void assume(boolean predicate) {
        assume(predicate, defaultMessageAssumption);
    }

    /**
     * Assumes the *conjunction* of all passed predicates. Throws a {@link ContractViolationException}
     * if *any* of the assumed predicates evaluates to {@code false}.
     * @param message The error message shown upon violation.
     * @param predicates The predicates to be assumed.
     */
    public static void assumeAll(String message, boolean... predicates) {
        for (boolean predicate : predicates) {
            assume(predicate, message);
        }
    }

    /**
     * Assumes the *conjunction* of all passed predicates. Throws a {@link ContractViolationException}
     * if *any* of the assumed predicates evaluates to {@code false}.
     * @param predicates The predicates to be assumed.
     */
    public static void assumeAll(boolean... predicates) {
        assumeAll(defaultMessageAssumption, predicates);
    }

    /**
     * Assumes the *disjunction* of all passed predicates. Throws a {@link ContractViolationException}
     * if *all* of the assumed predicates evaluate to {@code false}.
     * @param predicates The predicates to be assumed.
     */
    public static void assumeAny(boolean... predicates) {
        for (boolean p : predicates) {
            if (p) {
                return;
            }
        }
        throw new ContractViolationException(defaultMessageAssumption);
    }

    private static class ContractViolationException extends RuntimeException {
        private ContractViolationException(String message) {
            super(message);
        }
    }
}
