/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.searching;

import ltoorn.genix.TestDataHelper;
import ltoorn.genix.context.Context;
import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.dsl.shared.ValidationResult;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.dsl.specification.validation.SpecificationValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class SearchIndexElementsTest {

    // todo: Design proper test scaffolding, with varied and valid test data and with both a default context and a context with peculiar configuration.

    @Test
    void testSearchIndexElements() {

        Context.initialize(UserConfig.defaultUserConfig());

        // todo: expand test
        {
            List<String> pages = new ArrayList<>();
            pages.add("Who says calculating is better than scheming? syn0");
            pages.add("Scheming is fun. Though calculating does come in handy from time to time.");
            pages.add("When you need things (syn1) to be crystal clear.");
            pages.add("But, paraphrasing one of the original wizards: B ball of mud may not be pretty, but you can always add to a ball of mud.");
            pages.add("You can't really ad anything to a diamond. -- syn2");
            pages.add("But then again, how can you ever rely on a ball of mud. Is it even a ball? Don't forget to put in a reference!");

            ValidationResult<List<IndexElement>> result = SpecificationValidator.validate(TestDataHelper.richElementWithMembers() + TestDataHelper.quote.apply(TestDataHelper.SIMPLE));

            Assertions.assertTrue(result.isSuccessful());

            Assertions.assertTrue(result.getResult().get(0).getPages().isEmpty());

            SearchIndexElements.findOccurrences(result.getResult(), pages);

            // We ignore set operators for now, until we have better test scaffolding. See todo above.
            Set<Integer> matchedSynonymPages = new TreeSet<>();
            for (IndexElement parentSynonym : result.getResult().get(0).getSynonyms()) {
                matchedSynonymPages.addAll(parentSynonym.getPages());
            }

            Assertions.assertFalse(matchedSynonymPages.isEmpty());
            Assertions.assertTrue(matchedSynonymPages.contains(0));
            Assertions.assertTrue(matchedSynonymPages.contains(2));
            Assertions.assertTrue(matchedSynonymPages.contains(4));
        }
    }

    @Test
    void testFindElementContentOnPage() {
        {
            final int[] pageCodePoints = SearchUtil.stripHyphens("First\nword, second-word. Beginning \n\n\t of the second sentence." +
                    " Third\t sentence with 'single quote'. \"And a double quote\"." +
                    " Plus a reference (Someone 1923). \nLast word. a-hyphen. word-\nbreak")
                    .codePoints().toArray();

            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("first word", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("beginning", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("third sentence", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("'single quote'", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("single quote", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("\"and a double quote\"", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("and a double quote", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("someone", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("last word", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("1923", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("beginning of", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("ahyphen", pageCodePoints));
            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("wordbreak", pageCodePoints));

            Assertions.assertFalse(SearchIndexElements.findElementContentOnPage("ord", pageCodePoints));
            Assertions.assertFalse(SearchIndexElements.findElementContentOnPage("fir", pageCodePoints));
            Assertions.assertFalse(SearchIndexElements.findElementContentOnPage("ast word", pageCodePoints));
        }
        {
            final int[] pageCodePoints = SearchUtil.stripHyphens("This also means\n" +
                    "there must be some action φ that the agent can perform to bring that\n" +
                    "outcome about.").codePoints().toArray();

            Assertions.assertTrue(SearchIndexElements.findElementContentOnPage("action φ", pageCodePoints));
        }
    }
}
