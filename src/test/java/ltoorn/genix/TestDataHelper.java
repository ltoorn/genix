/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix;

import ltoorn.genix.dsl.specification.ir.AttributeKey;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.dsl.specification.parsing.Token;

import java.util.ArrayList;
import java.util.function.Function;

public class TestDataHelper {

    private TestDataHelper() {}

    public static final Function<String, String> quote = s -> "\"" + s + "\"";

    public static final String SIMPLE = "a simple element";
    public static final String WITH_REFERENCE = "an element with reference";
    public static final String REFERENCE = "a simple element";
    public static final String WITH_MEMBERS = "with members";
    public static final String WITH_SYNONYMS = "with synonyms";

    public static String richElementWithMembers() {
        String simple = quote.apply(SIMPLE);
        String withReference = quote.apply(WITH_REFERENCE) + " with { :hardReference = " + quote.apply(REFERENCE) + " }";
        String synonymsList = " (" + " @intersection " + quote.apply("syn0") + ", " + " @complement " + quote.apply("syn1") + ", " + quote.apply("syn2") + ")";
        String withSynonyms = quote.apply(WITH_SYNONYMS) + synonymsList;
        String withSimpleMembers = quote.apply("with simple members") + " with { :members = (" + " @right " + quote.apply("mem0") + ", " + " @leftOrRight" + quote.apply("mem1") + ") }";

        String withMembers = quote.apply(WITH_MEMBERS)
                + synonymsList
                + " with { :members = ("
                + " @left "
                + simple
                + ", "
                + withReference
                + ", "
                + withSynonyms
                + ", "
                + withSimpleMembers
                + ") }";

        return withMembers;
    }

    public static IndexElement buildElement(String content) {
        return IndexElement.of(new Token(Token.Type.LITERAL, content, 0));
    }

    public static ArrayList<IndexElement> buildElements(String baseName, int nrOfElements) {
        ArrayList<IndexElement> elements = new ArrayList<>();
        for (int i = 0; i < nrOfElements; i++) {
            elements.add(buildElement(baseName + "_" + i));
        }
        return elements;
    }

    public static <T> IndexElement assocAttribute(IndexElement element, AttributeKey key, T attribute) {
        element.assoc(key, attribute);
        return element;
    }
}
