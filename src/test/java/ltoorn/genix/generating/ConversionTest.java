/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ltoorn.genix.generating;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversionTest {

    @Test
    void testToRomanNumeral() {
        String[] numerals = {
                "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x", "xi", "xii", "xiii", "xiv", "xv",
                "xvi", "xvii", "xviii", "xix", "xx", "xxi", "xxii", "xxiii", "xxiv", "xxv", "xxvi", "xxvii", "xxviii",
                "xxix", "xxx",
        };
        for (int i = 0; i < numerals.length; ++i) {
            Assertions.assertEquals(numerals[i], Conversion.toRomanNumeral(i+1));
        }
        Assertions.assertEquals("xl", Conversion.toRomanNumeral(40));
        Assertions.assertEquals("xli", Conversion.toRomanNumeral(41));
        Assertions.assertEquals("xliv", Conversion.toRomanNumeral(44));
        Assertions.assertEquals("xlviii", Conversion.toRomanNumeral(48));
        Assertions.assertEquals("xlix", Conversion.toRomanNumeral(49));
        Assertions.assertEquals("li", Conversion.toRomanNumeral(51));
        Assertions.assertEquals("lxix", Conversion.toRomanNumeral(69));
        Assertions.assertEquals("xcviii", Conversion.toRomanNumeral(98));
        Assertions.assertEquals("xcix", Conversion.toRomanNumeral(99));
        Assertions.assertEquals("civ", Conversion.toRomanNumeral(104));
        Assertions.assertEquals("cix", Conversion.toRomanNumeral(109));
        Assertions.assertEquals("cmxcix", Conversion.toRomanNumeral(999));
        Assertions.assertEquals("mmm", Conversion.toRomanNumeral(3000));
    }
}
