/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.generating;

import ltoorn.genix.TestDataHelper;
import ltoorn.genix.dsl.specification.ir.AttributeKey;
import ltoorn.genix.dsl.specification.ir.Directive;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

public class IndexElementPreProcessorTest {

    @Test
    void testFinalizeIndexElements() {
        {
            List<IndexElement> elements = TestDataHelper.buildElements("test", 3);
            elements.get(0).addPage(1);
            elements.get(2).addPage(1);
            elements.get(0).assoc(AttributeKey.SOFT_REFERENCE, elements.get(1));

            BiFunction<List<IndexElement>, String, IndexElement> findByContent =
                    (l, c) ->
                            l.stream().filter(e -> e.contentForOutput.equalsIgnoreCase(c))
                                    .findFirst()
                                    .orElseThrow(RuntimeException::new);

            Assertions.assertEquals(3, elements.size());
            Assertions.assertTrue(findByContent.apply(elements, "test_0").hasSoftReference());

            List<IndexElement> finalized = IndexElementsPreProcessor.finalizeIndexElements(elements);

            Assertions.assertEquals(2, finalized.size());
            Assertions.assertFalse(findByContent.apply(finalized, "test_0").hasSoftReference());
        }
        {
            List<IndexElement> elements = new ArrayList<>();
            IndexElement parent = TestDataHelper.assocAttribute(TestDataHelper.buildElement("parent"), AttributeKey.INDEXABLE, false);
            IndexElement member = TestDataHelper.buildElement("member");
            IndexElement memberSynonym = TestDataHelper.assocAttribute(TestDataHelper.buildElement("synonym"), AttributeKey.SET_OPERATOR, Directive.SET_UNION);

            memberSynonym.addPage(1);
            member.addSynonym(memberSynonym);

            parent.addMember(member);
            elements.add(parent);

            Assertions.assertEquals(1, elements.size());
            Assertions.assertFalse(elements.get(0).isIndexable());
            Assertions.assertTrue(elements.get(0).getMembers().get(0).getPages().isEmpty());

            List<IndexElement> finalized = IndexElementsPreProcessor.finalizeIndexElements(elements);

            Assertions.assertEquals(1, finalized.size());
            Assertions.assertFalse(elements.get(0).isIndexable());
            Assertions.assertFalse(elements.get(0).getMembers().get(0).getPages().isEmpty());
        }
    }
}
