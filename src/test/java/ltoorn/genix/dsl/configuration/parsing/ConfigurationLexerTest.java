/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.parsing;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigurationLexerTest {

    @Test
    void testNextToken() {

        String configuration = "// A comment\n"
                + "pageNumbersStartDelimiter = \">>\"\n"
                + "pageNumberSeparator = \";\"\n"
                + "referenceDelimiter = \"see:\"\n"
                + "indexHeaderSpacing = \"\\n\\t--\\n\\n\"\n"
                + "// another comment\n"
                + "numberOfLeadingDummyPages = 0\n"
                + "\tindexableRange = (1,46)\n" // with extra whitespace
                + "romanNumeralEnd = 14 " // without newline
                + "isRomanNumeralUpperCase = false " // without newline
                + "sequenceConnector =\n\t \"-\"\n"; // with extra whitespace

        ParseResult parseResult = new ParseResult();
        ConfigurationLexer lexer = new ConfigurationLexer(configuration, parseResult);

        {
            Token token = lexer.nextToken();
            assertEquals("pageNumbersStartDelimiter", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(2, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(2, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals(">>", token.text);
            assertEquals(Token.Type.STRING, token.type);
            assertEquals(2, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("pageNumberSeparator", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(3, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(3, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals(";", token.text);
            assertEquals(Token.Type.STRING, token.type);
            assertEquals(3, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("referenceDelimiter", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(4, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(4, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("see:", token.text);
            assertEquals(Token.Type.STRING, token.type);
            assertEquals(4, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("indexHeaderSpacing", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(5, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(5, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("\n\t--\n\n", token.text);
            assertEquals(Token.Type.STRING, token.type);
            assertEquals(5, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("numberOfLeadingDummyPages", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(7, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(7, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("0", token.text);
            assertEquals(Token.Type.INTEGER, token.type);
            assertEquals(7, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("indexableRange", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("(", token.text);
            assertEquals(Token.Type.OPEN_PAREN, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("1", token.text);
            assertEquals(Token.Type.INTEGER, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals(",", token.text);
            assertEquals(Token.Type.COMMA, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("46", token.text);
            assertEquals(Token.Type.INTEGER, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals(")", token.text);
            assertEquals(Token.Type.CLOSE_PAREN, token.type);
            assertEquals(8, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("romanNumeralEnd", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("14", token.text);
            assertEquals(Token.Type.INTEGER, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("isRomanNumeralUpperCase", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("false", token.text);
            assertEquals(Token.Type.BOOLEAN, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("sequenceConnector", token.text);
            assertEquals(Token.Type.OPTION_NAME, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("=", token.text);
            assertEquals(Token.Type.EQ, token.type);
            assertEquals(9, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("-", token.text);
            assertEquals(Token.Type.STRING, token.type);
            assertEquals(10, token.line);
        }
        {
            Token token = lexer.nextToken();
            assertEquals("<end of input>", token.text);
            assertEquals(Token.Type.EOF, token.type);
            assertEquals(11, token.line);
        }

        assertTrue(parseResult.getRecognitionErrors().isEmpty());
    }

    @Test
    void testNextTokenError() {
        {
            String input = "foo_bar";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "1foo";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "1_";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "1.";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "[";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"\n\"";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"\\m\"";
            ParseResult result = new ParseResult();
            ConfigurationLexer lexer = new ConfigurationLexer(input, result);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
    }
}
