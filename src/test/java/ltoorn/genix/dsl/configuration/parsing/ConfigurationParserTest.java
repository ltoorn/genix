/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.parsing;

import ltoorn.genix.context.configuration.ConfigOption;
import ltoorn.genix.dsl.configuration.ir.ConfigOptionValueResult;
import ltoorn.genix.util.Pair;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ConfigurationParserTest {

    // todo: Test error handling.

    @Test
    void testParse() {

        String configuration = "// A comment\n"
                + "pageNumbersStartDelimiter \n= \",\"\n" // with extra newline
                + "pageNumberSeparator = \",\"\n"
                + "hardReferenceDelimiter = \"see:\"\n"
                + "softReferenceDelimiter = \"see also:\"\n"
                + "indexHeaderSpacing = \"\\n\"\n"
                + "// another comment\n"
                + "numberOfLeadingDummyPages = 0\n"
                + "indexableRange = (1,46)\n"
                + "romanNumeralEnd = 14 \n"
                + "isRomanNumeralUpperCase = false\n"
                + "sequenceConnector = \"-\"";

        ConfigurationParser parser = new ConfigurationParser(configuration);
        ParseResult result = parser.parse();

        assertTrue(result.getRecognitionErrors().isEmpty());

        ConfigOptionValueResult[] expectedResults = {
                createExpectedResult(ConfigOption.PAGE_NUMBERS_START_DELIMITER, 2, Pair.of(ConfigOption.ValueType.STRING, ",")),
                createExpectedResult(ConfigOption.PAGE_NUMBER_SEPARATOR, 4, Pair.of(ConfigOption.ValueType.STRING,  ",")),
                createExpectedResult(ConfigOption.HARD_REFERENCE_DELIMITER, 5, Pair.of(ConfigOption.ValueType.STRING, "see:")),
                createExpectedResult(ConfigOption.SOFT_REFERENCE_DELIMITER, 6, Pair.of(ConfigOption.ValueType.STRING, "see also:")),
                createExpectedResult(ConfigOption.INDEX_HEADER_SPACING, 7, Pair.of(ConfigOption.ValueType.STRING, "\n")),
                createExpectedResult(ConfigOption.NUMBER_OF_LEADING_DUMMY_PAGES, 9, Pair.of(ConfigOption.ValueType.INTEGER, 0)),
                createExpectedResult(ConfigOption.INDEXABLE_RANGE, 10, Pair.of(ConfigOption.ValueType.PAIR, Pair.of(1, 46))),
                createExpectedResult(ConfigOption.ROMAN_NUMERAL_END, 11, Pair.of(ConfigOption.ValueType.INTEGER, 14)),
                createExpectedResult(ConfigOption.IS_ROMAN_NUMERAL_UPPER_CASE, 12, Pair.of(ConfigOption.ValueType.BOOLEAN, false)),
                createExpectedResult(ConfigOption.SEQUENCE_CONNECTOR, 13, Pair.of(ConfigOption.ValueType.STRING, "-"))
        };

        List<ConfigOptionValueResult> actualResults = result.getConfigOptions();

        assertEquals(expectedResults.length, actualResults.size());

        for (int i = 0; i < expectedResults.length; i++) {
            assertEquals(expectedResults[i].configOption, actualResults.get(i).configOption);
            assertEquals(expectedResults[i].configOptionLineNr, actualResults.get(i).configOptionLineNr);
            assertEquals(expectedResults[i].typeValuePair.getLeft(), actualResults.get(i).typeValuePair.getLeft());
            assertEquals(expectedResults[i].typeValuePair.getRight(), actualResults.get(i).typeValuePair.getRight());
        }
    }

    private ConfigOptionValueResult createExpectedResult(ConfigOption option, int lineNr, Pair<ConfigOption.ValueType, Object> typeValuePair) {
        ConfigOptionValueResult configOptionValueResult = new ConfigOptionValueResult();
        configOptionValueResult.configOption = option;
        configOptionValueResult.configOptionLineNr = lineNr;
        configOptionValueResult.typeValuePair = typeValuePair;
        return configOptionValueResult;
    }
}
