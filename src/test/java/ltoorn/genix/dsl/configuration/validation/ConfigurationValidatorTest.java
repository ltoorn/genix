/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.configuration.validation;

import ltoorn.genix.context.configuration.UserConfig;
import ltoorn.genix.dsl.shared.ValidationResult;
import ltoorn.genix.util.Pair;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigurationValidatorTest {

    // todo: Test error handling.

    @Test
    void testValidate() {

        String configuration = "// A comment\n"
                + "pageNumbersStartDelimiter = \">>\"\n"
                + "pageNumberSeparator = \";\"\n"
                + "hardReferenceDelimiter = \"SEE:\"\n"
                + "softReferenceDelimiter = \"SEE ALSO:\"\n"
                + "indexHeaderSpacing = \"\\n\\t--\\n\\n\"\n"
                + "numberOfLeadingDummyPages = 3\n"
                + "indexableRange = (1,46)\n"
                + "romanNumeralEnd = 14\n"
                + "isRomanNumeralUpperCase = true\n"
                + "sequenceConnector = \"|\"";

        ValidationResult<UserConfig> result = ConfigurationValidator.validate(configuration);
        assertTrue(result.isSuccessful());

        UserConfig userConfig = result.getResult();
        assertEquals(">>", userConfig.getPageNumbersStartDelimiter());
        assertEquals(";", userConfig.getPageNumberSeparator());
        assertEquals("SEE:", userConfig.getHardReferenceDelimiter());
        assertEquals("SEE ALSO:", userConfig.getSoftReferenceDelimiter());
        assertEquals("\n\t--\n\n", userConfig.getIndexHeaderSpacing());
        assertEquals(3, userConfig.getNumberOfLeadingDummyPages());
        assertEquals(Pair.of(1, 46), userConfig.getIndexableRange());
        assertEquals(14, userConfig.getRomanNumeralEnd());
        assertEquals(true, userConfig.getRomanNumeralUpperCase());
        assertEquals("|", userConfig.getSequenceConnector());
    }
}
