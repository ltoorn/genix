/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.validation;

import ltoorn.genix.dsl.specification.ir.IndexElement;
import ltoorn.genix.dsl.shared.ValidationResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class SpecificationValidatorTest {

	@Test
	void testValidate() {
		String specification = "\"baz\" \"foo\"\n// this is a comment\n"
				+ "\"bar\" with { :hardReference = \"baz\" }\n\"composite\" with { :members = (\"comp1\", \"comp2\") }\n"
				+ "\"synonyms\" (\"eq1\", \"eq2\")\n";

		ValidationResult<List<IndexElement>> validationResult = SpecificationValidator.validate(specification);
		Assertions.assertTrue(validationResult.isSuccessful());

		List<IndexElement> elements = validationResult.getResult();
		Assertions.assertFalse(elements.isEmpty());

		Iterator<IndexElement> iterator = elements.iterator();
		Consumer<String> test = text -> Assertions.assertEquals(text, iterator.next().contentForSearch);

        test.accept("baz");
		test.accept("foo");
		test.accept("bar");
		test.accept("composite");
		test.accept("synonyms");
	}

	@Test
	void testValidateError() {
		{
			String input = "\"foo\"\n\"bar\" with { :hardReference = \"baz\" }";
			ValidationResult<List<IndexElement>> validationResult = SpecificationValidator.validate(input);
			Assertions.assertFalse(validationResult.isSuccessful());
			Assertions.assertEquals(1, validationResult.getErrors().size());
		}
		{
			String input = "\"foo\"\n\"foo\"";
			ValidationResult<List<IndexElement>> validationResult = SpecificationValidator.validate(input);
			Assertions.assertFalse(validationResult.isSuccessful());
			Assertions.assertEquals(1, validationResult.getErrors().size());
		}
		{
			String input = "\"foo\"\n\"bar\" with { :indexable = true, :hardReference = \"foo\" }";
			ValidationResult<List<IndexElement>> validationResult = SpecificationValidator.validate(input);
			Assertions.assertTrue(validationResult.isSuccessful()); // This only results in a warning.
			Assertions.assertEquals(1, validationResult.getWarnings().size());
		}
	}
}
