/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.parsing;

import org.junit.jupiter.api.Test;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

public class SpecificationLexerTest {

    private final Function<Token.Type, Token> ofType = t -> new Token(t, t.text, 0);
    private final BiFunction<Token.Type, String, Token> withText = (t, s) -> new Token(t, s, 0);

    private final BiConsumer<Token[], SpecificationLexer> tokenStreamConsumer = (expectedTokenStream, lexer) -> {
        for (Token expected : expectedTokenStream) {
            Token actual = lexer.nextToken();
            assertEquals(expected.type, actual.type);
            assertEquals(expected.text, actual.text);
        }
    };

    @Test
    void testNextToken() {

        ParseResult parseResult = new ParseResult();

        Function<String, String> stripQuotes = s -> s.substring(1, s.length()-1);
        Function<String, String> unEscape = s -> s.replace("\\\\", "\\").replace("\\\"", "\"");

        {
            String input = "\"a single element\"";
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(input)),
                    ofType.apply(Token.Type.EOF)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
        {
            String input = "\"with \\\"escape '\\\\' sequence\\\"\"";
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, unEscape.apply(stripQuotes.apply(input))),
                    ofType.apply(Token.Type.EOF)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
        {
            String input = "     \n \t\t       \"with leading and trailing whitespace\"  \n";

            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(input.trim())),
                    ofType.apply(Token.Type.EOF)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
        {
            String element = "\"a single element with comments\"";
            String input = "// first comment\n" + element + "// second comment\n// third comment";

            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(element)),
                    ofType.apply(Token.Type.EOF)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
        {
            String element = "\"an element\"";
            String reference = "\"with reference\"";
            String attributeBlock = " with { :hardReference = " + reference + " }";
            String input = element + attributeBlock;

            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(element)),
                    ofType.apply(Token.Type.KEYWORD_WITH),
                    ofType.apply(Token.Type.OPEN_CURLY),
                    withText.apply(Token.Type.ATTRIBUTE, "hardReference"),
                    ofType.apply(Token.Type.EQ),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(reference)),
                    ofType.apply(Token.Type.CLOSE_CURLY),
                    ofType.apply(Token.Type.EOF)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
        {
            String element = "\"with members\"";
            String parentSynonym = "\"parent synonym\"";
            String member0 = "\"member0\"";
            String member1 = "\"member1\"";
            String member2 = "\"member2\"";
            String attributeBlock = " with { :members = (@right " + member0 + "," + member1 + ", @leftOrRight " + member2 + ") }";
            String input = element + " (" + parentSynonym + ") " + attributeBlock;

            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(element)),
                    ofType.apply(Token.Type.OPEN_PAREN),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(parentSynonym)),
                    ofType.apply(Token.Type.CLOSE_PAREN),
                    ofType.apply(Token.Type.KEYWORD_WITH),
                    ofType.apply(Token.Type.OPEN_CURLY),
                    withText.apply(Token.Type.ATTRIBUTE, "members"),
                    ofType.apply(Token.Type.EQ),
                    ofType.apply(Token.Type.OPEN_PAREN),
                    withText.apply(Token.Type.DIRECTIVE, "right"),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(member0)),
                    ofType.apply(Token.Type.COMMA),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(member1)),
                    ofType.apply(Token.Type.COMMA),
                    withText.apply(Token.Type.DIRECTIVE, "leftOrRight"),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(member2)),
                    ofType.apply(Token.Type.CLOSE_PAREN),
                    ofType.apply(Token.Type.CLOSE_CURLY),
                    new Token(Token.Type.EOF, Token.Type.EOF.text, 0)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
        {
            String element = "\"with synonyms\"";
            String synonym0 = "\"synonym0\"";
            String synonym1 = "\"synonym1\"";
            String synonym2 = "\"synonym2\"";
            String input = element + " (" + "@union " + synonym0 + ", " + synonym1 + ", " + "@intersection " + synonym2 + ")\n";

            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(element)),
                    ofType.apply(Token.Type.OPEN_PAREN),
                    withText.apply(Token.Type.DIRECTIVE, "union"),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(synonym0)),
                    ofType.apply(Token.Type.COMMA),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(synonym1)),
                    ofType.apply(Token.Type.COMMA),
                    withText.apply(Token.Type.DIRECTIVE, "intersection"),
                    withText.apply(Token.Type.LITERAL, stripQuotes.apply(synonym2)),
                    ofType.apply(Token.Type.CLOSE_PAREN),
                    ofType.apply(Token.Type.EOF)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertTrue(parseResult.getRecognitionErrors().isEmpty());
        }
    }

    @Test
    void testNextTokenError() {
        {
            String input = "\"an element\" with { anError = \"foo\" }";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);

            Token[] expectedTokenStream = {
                    withText.apply(Token.Type.LITERAL, "an element"),
                    ofType.apply(Token.Type.KEYWORD_WITH),
                    ofType.apply(Token.Type.OPEN_CURLY),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.LEXER_ERROR),
                    ofType.apply(Token.Type.EQ),
                    withText.apply(Token.Type.LITERAL, "foo"),
                    ofType.apply(Token.Type.CLOSE_CURLY)
            };

            tokenStreamConsumer.accept(expectedTokenStream, lexer);

            assertFalse(parseResult.getRecognitionErrors().isEmpty());

            assertEquals(7, parseResult.getRecognitionErrors().size());
        }
        {
            String input = "\"an\nelement\"";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(parseResult.getRecognitionErrors().isEmpty());
            assertEquals(1, parseResult.getRecognitionErrors().size());
        }
        {
            String input = "\"with illegal escape\\n sequence\"";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(parseResult.getRecognitionErrors().isEmpty());
            assertEquals(1, parseResult.getRecognitionErrors().size());
        }
        {
            String input = "\"with illegal escape\\t sequence\"";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(parseResult.getRecognitionErrors().isEmpty());
            assertEquals(1, parseResult.getRecognitionErrors().size());
        }
        {
            String input = "'";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(parseResult.getRecognitionErrors().isEmpty());
            assertEquals(1, parseResult.getRecognitionErrors().size());
        }
        {
            String input = "&";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(parseResult.getRecognitionErrors().isEmpty());
            assertEquals(1, parseResult.getRecognitionErrors().size());
        }
        {
            String input = "]";

            ParseResult parseResult = new ParseResult();
            SpecificationLexer lexer = new SpecificationLexer(input, parseResult);
            assertEquals(Token.Type.LEXER_ERROR, lexer.nextToken().type);
            assertFalse(parseResult.getRecognitionErrors().isEmpty());
            assertEquals(1, parseResult.getRecognitionErrors().size());
        }
    }
}
