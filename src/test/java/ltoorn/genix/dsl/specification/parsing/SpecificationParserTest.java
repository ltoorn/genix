/*
 * MIT License
 *
 * Copyright © 2020 Lucas Christiaan van den Toorn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the Software), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package ltoorn.genix.dsl.specification.parsing;

import ltoorn.genix.TestDataHelper;
import ltoorn.genix.dsl.specification.ir.Directive;
import ltoorn.genix.dsl.specification.ir.IndexElement;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;

public class SpecificationParserTest {

    @Test
    void testParse() {
        {
            String input = "\"a simple element\"";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult parseResult = parser.parse();

            assertTrue(parseResult.getRecognitionErrors().isEmpty());

            ArrayList<IndexElement> ir = parseResult.getResult();
            IndexElement element = ir.get(0);

            assertFalse(element.hasHardReference());
            assertFalse(element.hasMembers());
            assertFalse(element.hasSynonyms());
            assertEquals("a simple element", element.contentForOutput);
        }
        {
            String input = "\"a simple element\"\n\"another simple element\"";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult parseResult = parser.parse();

            assertTrue(parseResult.getRecognitionErrors().isEmpty());

            ArrayList<IndexElement> ir = parseResult.getResult();

            assertEquals("a simple element", ir.get(0).contentForOutput);

            assertEquals("another simple element", ir.get(1).contentForOutput);
        }
        {
            String input = "\"a simple element\"\n\"another simple element\"";
            input += "\"element-ref\" with { :hardReference = \"with reference\" }";

            SpecificationParser parser = new SpecificationParser(input);

            ParseResult parseResult = parser.parse();

            assertTrue(parseResult.getRecognitionErrors().isEmpty());

            ArrayList<IndexElement> ir = parseResult.getResult();

            assertEquals("a simple element", ir.get(0).contentForOutput);

            assertEquals("another simple element", ir.get(1).contentForOutput);

            assertEquals("element-ref", ir.get(2).contentForOutput);
            assertEquals("elementref", ir.get(2).contentForSearch);

            assertFalse(parseResult.getReferences().isEmpty());
            assertEquals("with reference", parseResult.getReferences().get(0).getRight().getRight());
        }
        {
            String input = "\"a simple element\"\n\"another simple element\"";

            String withReference = "\"element-ref\" with { :hardReference = \"with reference\" }";
            String withMembers = "\"parent\" with { :members = (\"first\", @right \"second\", @leftOrRight \"third\") }";

            input += withReference;
            input += withMembers;

            SpecificationParser parser = new SpecificationParser(input);

            ParseResult parseResult = parser.parse();

            assertTrue(parseResult.getRecognitionErrors().isEmpty());

            ArrayList<IndexElement> ir = parseResult.getResult();

            assertEquals("a simple element", ir.get(0).contentForOutput);

            assertEquals("another simple element", ir.get(1).contentForOutput);

            assertEquals("element-ref", ir.get(2).contentForOutput);
            assertEquals("elementref", ir.get(2).contentForSearch);

            assertFalse(parseResult.getReferences().isEmpty());
            assertEquals("with reference", parseResult.getReferences().get(0).getRight().getRight());

            IndexElement parent = ir.get(3);
            assertEquals("parent", parent.contentForOutput);
            assertTrue(parent.hasMembers());

            List<IndexElement> members = parent.getMembers();
            assertEquals("first", members.get(0).contentForOutput);
            assertEquals("second", members.get(1).contentForOutput);
            assertEquals(Directive.RIGHT, members.get(1).getPositionDirective());
            assertEquals("third", members.get(2).contentForOutput);
            assertEquals(Directive.LEFT_OR_RIGHT, members.get(2).getPositionDirective());
        }
        {
            String simple = TestDataHelper.quote.apply(TestDataHelper.SIMPLE);
            String withReference = TestDataHelper.quote.apply(TestDataHelper.WITH_REFERENCE) +
                    " with { :hardReference = " +
                    TestDataHelper.quote.apply(TestDataHelper.REFERENCE) +
                    "}";

            String withSynonyms = TestDataHelper.quote.apply(TestDataHelper.WITH_SYNONYMS) +
                    " ( " +
                    " @intersection " +
                    TestDataHelper.quote.apply("syn0") +
                    ", " +
                    " @complement " +
                    TestDataHelper.quote.apply("syn1") +
                    ", " +
                    TestDataHelper.quote.apply("syn2") + ") ";

            String withSimpleMembers = TestDataHelper.quote.apply("with simple members") +
                    " with { :members = (" +
                    " @right " +
                    TestDataHelper.quote.apply("mem0") +
                    ", " +
                    " @leftOrRight" +
                    TestDataHelper.quote.apply("mem1") +
                    ") }";

            String withMembers = TestDataHelper.quote.apply(TestDataHelper.WITH_MEMBERS) +
                    " with { :members = (" +
                    " @left " +
                    simple +
                    ", " +
                    withReference +
                    ", " +
                    withSynonyms +
                    ", " +
                    withSimpleMembers +
                    ") }";

            String input = simple + withReference + "\n\n\n" + withSynonyms + "\n\t" + withMembers + "\n\n";

            SpecificationParser parser = new SpecificationParser(input);

            ParseResult parseResult = parser.parse();

            assertTrue(parseResult.getRecognitionErrors().isEmpty());

            ArrayList<IndexElement> ir = parseResult.getResult();

            assertEquals(4, ir.size());

            assertEquals(TestDataHelper.SIMPLE, ir.get(0).contentForOutput);
            assertEquals(1, ir.get(0).lineNumber);

            assertEquals(TestDataHelper.WITH_REFERENCE, ir.get(1).contentForOutput);
            assertEquals(1, ir.get(1).lineNumber);

            assertEquals(TestDataHelper.WITH_SYNONYMS, ir.get(2).contentForOutput);
            assertEquals(4, ir.get(2).lineNumber);

            Consumer<ArrayList<IndexElement>> checkSynonymList = synonyms -> {
                assertEquals(3, synonyms.size());
                for (int i = 0; i < synonyms.size(); i++) {
                    assertEquals("syn" + i, synonyms.get(i).contentForOutput);
                }
                assertEquals(Directive.SET_INTERSECTION, synonyms.get(0).getSetOperatorDirective());
                assertEquals(Directive.SET_COMPLEMENT, synonyms.get(1).getSetOperatorDirective());
            };

            checkSynonymList.accept(ir.get(2).getSynonyms());

            assertEquals(TestDataHelper.WITH_MEMBERS, ir.get(3).contentForOutput);
            assertEquals(5, ir.get(3).lineNumber);

            ArrayList<IndexElement> memberList = ir.get(3).getMembers();
            assertEquals(4, memberList.size());

            assertEquals(TestDataHelper.SIMPLE, memberList.get(0).contentForOutput);
            assertEquals(Directive.LEFT, memberList.get(0).getPositionDirective());

            assertEquals(TestDataHelper.WITH_REFERENCE, memberList.get(1).contentForOutput);
            assertEquals(5, memberList.get(1).lineNumber);

            assertEquals(TestDataHelper.WITH_SYNONYMS, memberList.get(2).contentForOutput);

            checkSynonymList.accept(memberList.get(2).getSynonyms());

            assertEquals("with simple members", memberList.get(3).contentForOutput);

            ArrayList<IndexElement> nestedMemberList = memberList.get(3).getMembers();
            assertEquals(2, nestedMemberList.size());
            for (int i = 0; i < nestedMemberList.size(); i++) {
                assertEquals("mem" + i, nestedMemberList.get(i).contentForOutput);
            }
            assertEquals(Directive.RIGHT, nestedMemberList.get(0).getPositionDirective());
            assertEquals(Directive.LEFT_OR_RIGHT, nestedMemberList.get(1).getPositionDirective());
        }
        {
            String withMembers = TestDataHelper.richElementWithMembers();
            SpecificationParser parser = new SpecificationParser(withMembers);

            ParseResult parseResult = parser.parse();

            assertTrue(parseResult.getRecognitionErrors().isEmpty());

            ArrayList<IndexElement> ir = parseResult.getResult();

            assertEquals(1, ir.size());

            Consumer<ArrayList<IndexElement>> checkSynonymList = synonyms -> {
                assertEquals(3, synonyms.size());
                for (int i = 0; i < synonyms.size(); i++) {
                    assertEquals("syn" + i, synonyms.get(i).contentForOutput);
                }
                assertEquals(Directive.SET_INTERSECTION, synonyms.get(0).getSetOperatorDirective());
                assertEquals(Directive.SET_COMPLEMENT, synonyms.get(1).getSetOperatorDirective());
            };

            assertEquals(TestDataHelper.WITH_MEMBERS, ir.get(0).contentForOutput);

            // The synonyms associated with the parent element.
            checkSynonymList.accept(ir.get(0).getSynonyms());

            ArrayList<IndexElement> memberList = ir.get(0).getMembers();
            assertEquals(4, memberList.size());

            assertEquals(TestDataHelper.SIMPLE, memberList.get(0).contentForOutput);
            assertEquals(Directive.LEFT, memberList.get(0).getPositionDirective());

            assertEquals(TestDataHelper.WITH_REFERENCE, memberList.get(1).contentForOutput);

            assertEquals(TestDataHelper.WITH_SYNONYMS, memberList.get(2).contentForOutput);

            checkSynonymList.accept(memberList.get(2).getSynonyms());

            assertEquals("with simple members", memberList.get(3).contentForOutput);

            ArrayList<IndexElement> nestedMemberList = memberList.get(3).getMembers();
            assertEquals(2, nestedMemberList.size());
            for (int i = 0; i < nestedMemberList.size(); i++) {
                assertEquals("mem" + i, nestedMemberList.get(i).contentForOutput);
            }
            assertEquals(Directive.RIGHT, nestedMemberList.get(0).getPositionDirective());
            assertEquals(Directive.LEFT_OR_RIGHT, nestedMemberList.get(1).getPositionDirective());
        }
    }

    @Test
    void testParseError() {
        {
            String input = "\"an element\" { :indexable = true }";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" with { :nonExistentAttribute = false }";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" with { :hardReference = \"foo\"";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" (\"foo\",";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" (@nonExistentDirective \"foo\")";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" with { :members = (@nonExistentDirective \"foo\") }";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" with { :hardReference \"missing eq\" }";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" (\"foo\" \"missing comma\")";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
        {
            String input = "\"an element\" with { :hardReference = \"foo\" :softReference = \"missing comma\" }";
            SpecificationParser parser = new SpecificationParser(input);

            ParseResult result = parser.parse();
            assertFalse(result.getRecognitionErrors().isEmpty());
            assertEquals(1, result.getRecognitionErrors().size());
        }
    }
}
